# Mazooma Single API Client

The *Mazooma Single API Client* provides Merchants with a means to access Mazooma product functionality
such as processing Consumer payins and payouts, and creating transaction reports.

This client is for the Java programming language.  Java 8 or later is required.

See the [client implementation guide](https://developer.mazooma.com/introduction/codeguide-java) for details.

## Installation Instructions

### Prebuilt libraries

* Download the **JAR** files listed in the [Artifacts](#artifacts) section below

* Add the **JAR** files to your project's class path

### Build from source

* Clone this repository

* Build the **JAR** files with [Maven](https://maven.apache.org/) by executing ```mvn package``` from the project's root directory.
Output **JAR** files can be found in:

    + ```src/java/client-common/target```
    + ```src/java/reportingapi-client/target```
    + ```src/java/transactionapi-client/target```

* Add the **JAR** files to your project's class path

## Documentation 

[https://developer.mazooma.com](https://developer.mazooma.com/)

## License

[MIT License](LICENSE.txt)

## Artifacts

| Name | Filename | Version | Artifact | SHA1 |
|---|---|---|---|---|
| Client Common | client-common-1.4.8.jar  | 1.4.8  | [Download](https://single-api-client.s3.ca-central-1.amazonaws.com/client-common-1.4.8.jar) | 6C0AB5925848C377C7A7C5756CB0BC7A1B2ACCFC |
| Reporting API Client | reportingapi-client-1.4.8.jar  | 1.4.8 | [Download](https://single-api-client.s3.ca-central-1.amazonaws.com/reportingapi-client-1.4.8.jar) | 630AF0AD87E94E8FCD105494BC936F6547C8E264 |
| Transaction API Client | transactionapi-client-1.4.8.jar  | 1.4.8 | [Download](https://single-api-client.s3.ca-central-1.amazonaws.com/transactionapi-client-1.4.8.jar) | EBDDC974273D24DF83568A12733E8A956F81D148 |