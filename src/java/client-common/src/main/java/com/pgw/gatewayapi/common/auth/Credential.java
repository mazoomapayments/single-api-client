package com.pgw.gatewayapi.common.auth;

public class Credential {
    private long version;
    private OAuth2Token token;
    private long expiredAt;

    public Credential(long version, OAuth2Token token) {
        this.version = version;
        setToken(token);
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public OAuth2Token getToken() {
        return token;
    }

    public void setToken(OAuth2Token token) {
        this.token = token;
        expiredAt = token == null ? Long.MIN_VALUE : System.currentTimeMillis() + token.getExpiresIn() * 1000L;
    }

    public long getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(long expiredAt) {
        this.expiredAt = expiredAt;
    }
}
