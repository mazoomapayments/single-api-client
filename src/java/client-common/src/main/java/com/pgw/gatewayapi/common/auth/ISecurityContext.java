package com.pgw.gatewayapi.common.auth;

import com.pgw.gatewayapi.common.exception.AuthException;

/**
 * A security context that maintains the credential used to access the gateway api
 *  @author Lawrence Li
 */
public interface ISecurityContext {

    /**
     * Invalidate the current credential if its version matches the timestamp
     * @param version a version of the credential
     */
    void invalidate(long version);

    /**
     * Get a valid credential to access the transaction api
     *
     */
    Credential getCredential() throws AuthException;
}
