package com.pgw.gatewayapi.common.auth;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.pgw.gatewayapi.common.util.ScopeDeserializer;

import java.io.Serializable;
import java.util.Set;

public class OAuth2Token implements Serializable {

    private static final long serialVersionUID = -7462891244456560791L;
    private String accessToken;
    private long expiresIn;
    private String tokenType;

    @JsonDeserialize(using = ScopeDeserializer.class)
    private Set<String> scope;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public Set<String> getScope() {
        return scope;
    }

    public void setScope(Set<String> scope) {
        this.scope = scope;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    @Override
    public String toString() {
        return "OAuth2Token{" +
                "accessToken='" + accessToken + '\'' +
                ", expiredIn=" + expiresIn +
                ", scope='" + scope + '\'' +
                ", tokenType='" + tokenType + '\'' +
                '}';
    }
}
