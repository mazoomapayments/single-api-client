package com.pgw.gatewayapi.common.auth.impl;

import com.pgw.gatewayapi.common.auth.OAuth2Token;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface OAuth2ClientApi {

    @POST("oauth/token")
    @FormUrlEncoded
    Call<OAuth2Token> getToken(@Field("grant_type") String grantType,
            @Field("scope") String scope,
            @Field("client_id") String clientId,
            @Field("client_secret") String clientSecret);
}
