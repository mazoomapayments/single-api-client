package com.pgw.gatewayapi.common.auth.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.pgw.gatewayapi.common.auth.Credential;
import com.pgw.gatewayapi.common.auth.ISecurityContext;
import com.pgw.gatewayapi.common.auth.OAuth2Token;
import com.pgw.gatewayapi.common.exception.AuthException;
import com.pgw.gatewayapi.common.util.CommonUtils;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


class SecurityContext implements ISecurityContext {

    private static final Logger logger = LoggerFactory.getLogger(SecurityContext.class);

    private static final String GRANT_TYPE = "client_credentials";
    private String clientId;
    private String clientSecret;
    private OAuth2ClientApi api;
    private OAuth2Token token;
    private AtomicLong version = new AtomicLong(-1);

    SecurityContext(String authUrl, String clientId, String clientSecret, long timeout) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        api = createClient(authUrl, timeout);
    }

    @Override
    public void invalidate(long version) {
        this.version.compareAndSet(version, -1);
    }

    @Override
    public Credential getCredential() throws AuthException{
        Credential credential = new Credential(version.get(), token);
        if (credential.getVersion() == -1 || System.currentTimeMillis() >= credential.getExpiredAt()) {
            synchronized (this) {
                try {
                    credential = new Credential(version.get(), token);
                    if (credential.getVersion() == -1 || System.currentTimeMillis() >= credential.getExpiredAt()) {
                        /*
                         * When fetching new token process starts, force all other thread to wait till the process is done
                         */
                        version.set(-1);
                        logger.info("Fetch a new token");
                        Response<OAuth2Token> resp = api.getToken(GRANT_TYPE, null, clientId, clientSecret).execute();
                        if (resp.isSuccessful()) {
                            token = resp.body();
                            if (token == null) {
                                throw new AuthException("Failed to fetch oauth2 token");
                            }
                            credential.setToken(token);
                        } else {
                            throw new AuthException(CommonUtils.getErrorMsg(resp));
                        }
                        credential.setVersion(System.nanoTime());
                        version.set(credential.getVersion());
                        logger.info("Obtained a new token with version {}", credential.getVersion());
                    }

                    return credential;
                } catch (Throwable e) {
                    if (e instanceof AuthException) {
                        throw (AuthException) e;
                    }
                    throw new AuthException(e);
                }
            }
        }

        return credential;
    }

    private OAuth2ClientApi createClient(String authUrl, long timeout) {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .readTimeout(timeout, TimeUnit.SECONDS)
                .retryOnConnectionFailure(false);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create(mapper))
                .baseUrl(authUrl)
                .client(clientBuilder.build())
                .build();

        return retrofit.create(OAuth2ClientApi.class);
    }
}
