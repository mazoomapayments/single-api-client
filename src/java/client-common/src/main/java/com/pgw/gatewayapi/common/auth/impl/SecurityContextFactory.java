package com.pgw.gatewayapi.common.auth.impl;

import com.pgw.gatewayapi.common.auth.ISecurityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SecurityContextFactory {

    private static final Logger logger = LoggerFactory.getLogger(SecurityContextFactory.class);

    private String authUr;
    private String clientId;
    private String clientSecret;
    private long timeout = 60;

    public SecurityContextFactory(String authUr, String clientId, String clientSecret) {
        this.authUr = authUr;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }

    public SecurityContextFactory timeout(long timeout) {
        this.timeout = timeout;
        return this;
    }

    public ISecurityContext create() {
        logger.info("Build a security context");
        return new SecurityContext(authUr, clientId, clientSecret, timeout);
    }
}
