package com.pgw.gatewayapi.common.exception;

/**
 * An exception used when a ISecurityContext cannot get a credential
 * @author Lawrence Li
 */
public class AuthException extends Exception {

    public AuthException(String message) {
        super(message);
    }

    public AuthException(Throwable cause) {
        super(cause);
    }
}
