package com.pgw.gatewayapi.common.exception;

import com.pgw.gatewayapi.common.support.ErrorResponse;

import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class GateWayApiException extends Exception {
    private List<ErrorResponse> errorResponses;

    public GateWayApiException(List<ErrorResponse> errorResponses) {
        this.errorResponses = errorResponses;
    }

    public GateWayApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public List<ErrorResponse> getErrorResponses() {
        return errorResponses;
    }

    @Override
    public String getMessage() {
        if (errorResponses != null && !errorResponses.isEmpty()) {
            StringBuilder sbd = new StringBuilder("[\n");
            String content = errorResponses.stream()
                    .map(ErrorResponse::toString)
                    .collect(Collectors.joining(",\n"));
            sbd.append(content).append("\n]");
            return pretty(sbd.toString());
        }
        return super.getMessage();
    }

    private String pretty(String str) {
        StringTokenizer st = new StringTokenizer(str, "\n");
        StringBuilder sbd = new StringBuilder();
        int indent = 0;
        while (st.hasMoreTokens()) {
            String tk = st.nextToken();
            if (tk.startsWith("]") || tk.startsWith("}")) {
                indent--;
            }
            for (int i = 0; i < indent; i++) {
                sbd.append("\t");
            }
            sbd.append(tk).append("\n");
            if ("[".equals(tk) || "{".equals(tk)) {
                indent++;
            }
        }
        return sbd.toString();
    }
}
