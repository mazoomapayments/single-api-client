package com.pgw.gatewayapi.common.exception;


import com.pgw.gatewayapi.common.support.ErrorResponse;

import java.util.List;

public class RetryException extends RuntimeException {
    private List<ErrorResponse> errorResponses;

    public RetryException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public RetryException(List<ErrorResponse> errorResponses) {
        this.errorResponses = errorResponses;
    }

    public List<ErrorResponse> getErrorResponses() {
        return errorResponses;
    }
}
