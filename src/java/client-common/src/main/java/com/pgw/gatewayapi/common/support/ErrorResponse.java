package com.pgw.gatewayapi.common.support;

import java.io.Serializable;

public class ErrorResponse implements Serializable {

    private static final long serialVersionUID = 2593546069997303361L;

    private String errorCode;
    private String errorMessage;

    public static ErrorResponse of(String errorCode, String errorMessage) {
        ErrorResponse err = new ErrorResponse();
        err.errorCode = errorCode;
        err.errorMessage = errorMessage;
        return err;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "{\n"
                + "code: " + errorCode + ",\n"
                + "message: " + errorMessage + "\n"
                + "}";
    }
}
