package com.pgw.gatewayapi.common.support;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pgw.gatewayapi.common.exception.GateWayApiException;
import com.pgw.gatewayapi.common.exception.RetryException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import retrofit2.HttpException;
import retrofit2.Response;

/**
 * A wrapper around the CompletableFuture. It checks the result of the execution. If the returned http status code is 401 or 403, it triggers
 * a RetryException or if the http status code is any none-200 code, triggers a GateWayApiException.
 * @author Lawrence Li
 */
public class ExecutionResult<T> {

    private T result;
    private Throwable error;

    public ExecutionResult(Throwable error) {
        this.error = error;
    }

    public ExecutionResult(T result) {
        this.result = result;
    }

    public T getResult() {
        return result;
    }

    public Throwable getError() {
        return error;
    }

    public static <R> CompletableFuture<ExecutionResult<R>> wrap(CompletableFuture<R> exec) {
        CompletableFuture<ExecutionResult<R>> p = new CompletableFuture<>();
        exec.whenComplete((r, er) -> {
            if (er != null) {
                while (er instanceof CompletionException) {
                    er = er.getCause();
                }
                if (er instanceof HttpException) {
                    HttpException ex = (HttpException) er;
                    String errorBody = Optional.ofNullable(ex.response())
                            .map(Response::errorBody)
                            .map(by -> {
                                try {
                                    return by.string();
                                } catch (IOException e) {
                                    return null;
                                }
                            })
                            .orElse(null);
                    List<ErrorResponse> errors = parseError(errorBody);
                    if (ex.code() == 401 || ex.code() == 403) {
                        p.complete(new ExecutionResult<>(errors == null || errors.isEmpty() ? new RetryException(errorBody, er) : new RetryException(errors)));
                    } else {
                        p.complete(new ExecutionResult<>(errors == null || errors.isEmpty() ? new GateWayApiException(errorBody, er) : new GateWayApiException(errors)));
                    }
                    return;
                }
                p.complete(new ExecutionResult<>(er));
            } else {
                p.complete(new ExecutionResult<>(r));
            }
        });
        return p;
    }

    private static List<ErrorResponse> parseError(String error) {
        return Optional.ofNullable(error)
                .map(er -> {
                    ObjectMapper om = new ObjectMapper();
                    try {
                        return om.<List<ErrorResponse>>readValue(er, new TypeReference<List<ErrorResponse>>(){});
                    } catch (IOException e) {
                        return null;
                    }
                })
                .orElseGet(Collections::emptyList);

    }
}
