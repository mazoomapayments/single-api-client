package com.pgw.gatewayapi.common.support;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.pgw.gatewayapi.common.auth.ISecurityContext;
import com.pgw.gatewayapi.common.auth.impl.SecurityContextFactory;
import java.util.concurrent.TimeUnit;
import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Standard client factory template. It is the base class for transaction api client factory and reporting api client factory.
 * @author Lawrence Li
 **/
public abstract class StandardClientFactory<API, CLIENT> {
    private String endpoint;
    private String authUrl;
    private String clientId;
    private String clientSecret;
    private String appKey;
    private int maxThreads = 64;
    private long timeout = 60L;

    public StandardClientFactory(String endpoint, String authUrl, String clientId, String clientSecret, String appKey) {
        this.endpoint = endpoint;
        this.authUrl = authUrl;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.appKey = appKey;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public void setMaxThreads(int maxThreads) {
        this.maxThreads = maxThreads;
    }

    public CLIENT create() {
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setMaxRequestsPerHost(maxThreads);
        dispatcher.setMaxRequests(Math.max(dispatcher.getMaxRequests(), maxThreads));

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .readTimeout(timeout, TimeUnit.SECONDS)
                .retryOnConnectionFailure(false)
                .dispatcher(dispatcher);

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create(mapper))
                .baseUrl(endpoint)
                .client(clientBuilder.build())
                .build();

        API api = retrofit.create(getApiClass());

        SecurityContextFactory factory = new SecurityContextFactory(authUrl, clientId, clientSecret).timeout(timeout);
        ISecurityContext securityContext = factory.create();

        return createClient(api, securityContext, clientId, appKey);

    }

    public abstract Class<API> getApiClass();

    public abstract CLIENT createClient(API api, ISecurityContext securityContext, String clientId, String appKey);

}
