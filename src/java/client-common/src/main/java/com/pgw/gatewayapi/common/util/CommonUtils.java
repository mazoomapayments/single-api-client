package com.pgw.gatewayapi.common.util;

import com.pgw.gatewayapi.common.auth.Credential;
import com.pgw.gatewayapi.common.auth.ISecurityContext;
import com.pgw.gatewayapi.common.exception.AuthException;
import com.pgw.gatewayapi.common.exception.GateWayApiException;
import com.pgw.gatewayapi.common.exception.RetryException;
import com.pgw.gatewayapi.common.support.ExecutionResult;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import okhttp3.ResponseBody;
import org.slf4j.Logger;
import retrofit2.Response;

public class CommonUtils {

    public static String getErrorMsg(Response<?> response) throws IOException {
        ResponseBody error = response.errorBody();
        return "status code: " + response.code() + ", " + (error == null ? "n/a" : error.string());
    }

    public static <R> CompletableFuture<R> failFuture(Throwable ex) {
        CompletableFuture<R> future = new CompletableFuture<>();
        future.completeExceptionally(ex);
        return future;
    }

    public static  <T> T sync(CompletableFuture<T> async) throws GateWayApiException {
        try {
            return async.get();
        }catch (InterruptedException e) {
            throw new IllegalStateException(e);
        } catch (ExecutionException e) {
            if (e.getCause() instanceof GateWayApiException) {
                throw (GateWayApiException) e.getCause();
            }else if (e.getCause() instanceof RuntimeException) {
                throw (RuntimeException)e.getCause();
            }else {
                throw new IllegalStateException(e.getCause());
            }

        }
    }

    /**
     * execute a request and retry it if it throws a RetryException
     * @param logger A logger instance
     * @param auth the security context that can serve credential for the request
     * @param err The exception to throw when the request finally fails
     * @param cnt the max retry count
     * @param request the request
     */
    public static  <T> CompletableFuture<T> retry(Logger logger, ISecurityContext auth, Throwable err, int cnt, Function<Credential, CompletableFuture<T>> request) {
        if (cnt == 0) {
            logger.error("Unable to obtain a valid token");
            return CommonUtils.failFuture(err);
        }
        Credential credential;
        try {
            credential = auth.getCredential();
        } catch (AuthException e) {
            return CommonUtils.failFuture(e);
        }
        Credential c = credential;
        return ExecutionResult.wrap(request.apply(c))
                .thenCompose(r -> {
                    if (r.getError() != null) {
                        if (r.getError() instanceof RetryException) {
                            logger.error("Token expired, try again");
                            auth.invalidate(c.getVersion());
                            RetryException re = (RetryException)r.getError();
                            if (re.getErrorResponses() != null && !re.getErrorResponses().isEmpty()) {
                                return retry(logger, auth, new GateWayApiException(re.getErrorResponses()), cnt - 1, request);
                            }else {
                                return retry(logger, auth, new GateWayApiException(re.getMessage(), re.getCause()), cnt - 1, request);
                            }
                        } else {
                            return CommonUtils.failFuture(r.getError());
                        }
                    } else {
                        return CompletableFuture.completedFuture(r.getResult());
                    }
                });
    }

    public static String makeToken(Credential credential) {
        return credential.getToken().getTokenType() + " " + credential.getToken().getAccessToken();
    }
}
