package com.pgw.gatewayapi.common.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

/**
 * The oauth2 returns scopes as a string. The deserializer converts it to a set
 * @author Lawrence Li
 */
public class ScopeDeserializer extends StdDeserializer<Set<String>> {

    public ScopeDeserializer() {
        super(Set.class);
    }

    @Override
    public Set<String> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String txt = jsonParser.getText();
        if (txt == null || (txt = txt.trim()).isEmpty()) {
            return null;
        }

        StringTokenizer tokenizer = new StringTokenizer(txt);
        return Collections.list(tokenizer)
                .stream()
                .map(str -> ((String)str).trim())
                .collect(Collectors.toSet());
    }
}
