package com.pgw.gatewayapi.reporting;

import com.pgw.gatewayapi.common.auth.ISecurityContext;
import com.pgw.gatewayapi.common.support.StandardClientFactory;
import com.pgw.gatewayapi.reporting.impl.ReportingApi;
import com.pgw.gatewayapi.reporting.impl.ReportingClient;

/**
 * Client factory for reporting api. A reporting api client has to be created by this factory. The required arguments are: <p></p>
 * endpoint - the base URL of reporting api (https://staging.mazoomagateway.com/report/ and etc.) <p></p>
 * authUrl - the base URL of the oauth service (https://staging.mazoomagateway.com/register/ and etc.) <p></p>
 * clientId, clientSecret, appKey - The security credential issued by mazooma <p></p>
 *
 * To obtain a reporting api client
 * <pre>
 *     IReportingClient client = new ClientFactory(ENDPOINT, AUTHURL, CLIENTID, CLIENTSECRET, APPKEY).create();
 * </pre>
 *
 * @author Lawrence Li
 **/
public class ClientFactory extends StandardClientFactory<ReportingApi, IReportingClient> {

    public ClientFactory(String endpoint, String authUrl, String clientId, String clientSecret, String appKey) {
        super(endpoint, authUrl, clientId, clientSecret, appKey);
    }

    @Override
    public Class<ReportingApi> getApiClass() {
        return ReportingApi.class;
    }

    @Override
    public IReportingClient createClient(ReportingApi reportingApi, ISecurityContext securityContext, String clientId, String appKey) {
        return new ReportingClient(reportingApi, securityContext, clientId, appKey);
    }
}

