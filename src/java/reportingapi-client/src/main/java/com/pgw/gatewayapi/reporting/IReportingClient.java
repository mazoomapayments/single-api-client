package com.pgw.gatewayapi.reporting;

import com.pgw.gatewayapi.common.exception.GateWayApiException;
import com.pgw.gatewayapi.reporting.model.request.ReportRequest;
import com.pgw.gatewayapi.reporting.model.response.BaseReportResponse;
import java.io.InputStream;
import java.util.concurrent.CompletableFuture;

/**
 * An interface retrieving reports from the reporting api.
 *
 * @author Lawrence Li
 **/
public interface IReportingClient {

    /**
     * Download a report from server. The report is generated in accord to the specified request. <p></p>
     * The report can be with format of csv or json and can also be specified as compressed as zip or not.
     */
    InputStream getReportAsStream(ReportRequest request) throws GateWayApiException;

    CompletableFuture<InputStream> getReportAsStreamAsync(ReportRequest request);

    /**
     * Retrieve a report from server, return an iterable of report response.
     */
    <T extends BaseReportResponse> Iterable<T> getReport(ReportRequest request) throws GateWayApiException;

    <T extends BaseReportResponse> CompletableFuture<Iterable<T>> getReportAsync(ReportRequest request);
}
