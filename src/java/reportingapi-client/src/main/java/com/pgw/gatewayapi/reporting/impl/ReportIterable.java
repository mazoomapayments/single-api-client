package com.pgw.gatewayapi.reporting.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.pgw.gatewayapi.reporting.model.EOutputType;
import com.pgw.gatewayapi.reporting.model.EQueryType;
import com.pgw.gatewayapi.reporting.model.response.BaseReportResponse;
import com.pgw.gatewayapi.reporting.model.response.ReturnReportResponse;
import com.pgw.gatewayapi.reporting.model.response.TransactionReportResponse;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.zip.ZipInputStream;

class ReportIterable<T extends BaseReportResponse> implements Iterable<T> {

    private InputStream inputStream;
    private ObjectReader objectReader;

    private abstract class StreamIterator implements Iterator<T> {

        T item;

        @Override
        public boolean hasNext() {
            return item != null || (item = readNext()) != null;
        }

        @Override
        public T next() {
            T result = item;
            item = null;
            return result;
        }

        abstract T readNext();
    }

    private class JsonIterator extends StreamIterator {

        private boolean closed = false;

        private JsonIterator() {
            try {
                int st = inputStream.read();
                if (st != '[') {
                    finished();
                    throw new IllegalStateException("Not a valid report response");
                }
            } catch (IOException e) {
                throw new IllegalStateException("Not a valid report response", e);
            }
        }

        @Override
        T readNext() {
            try {
                return closed ? null : readResponse();
            } catch (IOException e) {
                throw new IllegalStateException("Not a valid report response", e);
            }
        }

        private T readResponse() throws IOException {
            int ch = inputStream.read();
            while (ch != -1 && (Character.isWhitespace(ch) || ch == ',')) {
                ch = inputStream.read();
            }
            if (ch == ']') {
                return finished();
            }
            if (ch != '{') {
                finished();
                throw new IllegalStateException("Not a valid report response");
            }
            int bracketCount = 1;
            StringBuilder sbd = new StringBuilder().append((char) ch);
            while (bracketCount > 0) {
                ch = inputStream.read();
                sbd.append((char) ch);
                if (ch == '{') {
                    bracketCount++;
                } else if (ch == '}') {
                    bracketCount--;
                } else if (ch == -1) {
                    finished();
                    throw new IllegalStateException("Not a valid report response");
                }
            }

            return objectReader.readValue(sbd.toString());
        }

        private T finished() throws IOException {
            closed = true;
            inputStream.close();
            return null;
        }
    }

    public ReportIterable(InputStream inputStream, boolean outputZipFile, EOutputType outputType, EQueryType queryType) {
        if (outputType != EOutputType.JSON) {
            throw new UnsupportedOperationException("'" + outputType + "' cannot be converted to Java object");
        }
        this.inputStream = new BufferedInputStream(inputStream);
        if (outputZipFile) {
            try {
                ZipInputStream zip = new ZipInputStream(inputStream);
                zip.getNextEntry();
                this.inputStream = zip;
            } catch (IOException e) {
                throw new IllegalStateException("Not a valid report response");
            }
        }
        ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .registerModule(new JavaTimeModule());
        objectReader = queryType == EQueryType.TRANSACTION? mapper.readerFor(TransactionReportResponse.class) : mapper.readerFor(ReturnReportResponse.class);
    }

    @Override
    public Iterator<T> iterator() {
        return new JsonIterator();
    }
}
