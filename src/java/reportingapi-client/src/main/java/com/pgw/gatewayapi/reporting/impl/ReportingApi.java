package com.pgw.gatewayapi.reporting.impl;

import com.pgw.gatewayapi.reporting.model.request.ReportRequest;
import java.util.concurrent.CompletableFuture;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Streaming;

public interface ReportingApi {

    @POST("v4/report")
    @Streaming
    CompletableFuture<ResponseBody> report(@Header("x-pgw-client-id") String clientId,
            @Header("x-pgw-app-id") String appKey,
            @Header("Authorization") String token,
            @Body ReportRequest request);
}
