package com.pgw.gatewayapi.reporting.impl;

import static com.pgw.gatewayapi.common.util.CommonUtils.makeToken;
import static com.pgw.gatewayapi.common.util.CommonUtils.retry;
import static com.pgw.gatewayapi.common.util.CommonUtils.sync;

import com.pgw.gatewayapi.common.auth.Credential;
import com.pgw.gatewayapi.common.auth.ISecurityContext;
import com.pgw.gatewayapi.common.exception.GateWayApiException;
import com.pgw.gatewayapi.reporting.IReportingClient;
import com.pgw.gatewayapi.reporting.model.EOutputType;
import com.pgw.gatewayapi.reporting.model.request.ReportRequest;
import com.pgw.gatewayapi.reporting.model.response.BaseReportResponse;
import java.io.InputStream;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import okhttp3.ResponseBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportingClient implements IReportingClient {

    private static final Logger logger = LoggerFactory.getLogger(ReportingClient.class);

    private ReportingApi api;
    private ISecurityContext auth;
    private String clientId;
    private String appKey;

    public ReportingClient(ReportingApi api, ISecurityContext auth, String clientId, String appKey) {
        this.api = api;
        this.auth = auth;
        this.clientId = clientId;
        this.appKey = appKey;
    }

    @Override
    public InputStream getReportAsStream(ReportRequest request) throws GateWayApiException {
        return sync(getReportAsStreamAsync(request));
    }

    @Override
    public CompletableFuture<InputStream> getReportAsStreamAsync(ReportRequest request) {
        Objects.requireNonNull(request, "Report request is required");
        Objects.requireNonNull(request.getReport(), "Report search condition is required");
        Objects.requireNonNull(request.getReport().getQueryType(), "Report query type (TRANSACTION or RETURN) is required");
        Objects.requireNonNull(request.getReport().getStartDateTime(), "Report start time is required");
        Objects.requireNonNull(request.getReport().getEndDateTime(), "Report finish time is required");

        if (request.getReport().getOutputType() == null) {
            request.getReport().setOutputType(EOutputType.JSON);
        }

        return retry(logger, auth, null, 2, credential -> reportStream(request, credential));
    }

    @Override
    public <T extends BaseReportResponse> Iterable<T> getReport(ReportRequest request) throws GateWayApiException {
        return sync(getReportAsync(request));
    }

    @Override
    public <T extends BaseReportResponse> CompletableFuture<Iterable<T>> getReportAsync(ReportRequest request) {
        return getReportAsStreamAsync(request)
                .thenApply(in -> new ReportIterable<>(in, request.getReport().isOutputZipFile(), request.getReport().getOutputType(), request.getReport().getQueryType()));
    }

    private CompletableFuture<InputStream> reportStream(ReportRequest request, Credential credential) {
        return api.report(clientId, appKey, makeToken(credential), request)
                .thenApply(ResponseBody::byteStream);
    }
}
