package com.pgw.gatewayapi.reporting.model;

public enum EDeviceType {
    DESKTOP, MOBILE, TABLET, UNKNOWN
}
