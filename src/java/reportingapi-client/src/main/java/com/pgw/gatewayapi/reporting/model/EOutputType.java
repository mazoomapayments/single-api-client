package com.pgw.gatewayapi.reporting.model;

public enum EOutputType {
    JSON, CSV
}
