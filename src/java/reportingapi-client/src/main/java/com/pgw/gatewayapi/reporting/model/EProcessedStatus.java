package com.pgw.gatewayapi.reporting.model;

public enum EProcessedStatus {
    PENDING, SUCCESSFUL, FAILED, REJECTED, DECLINED, CANCELED, ABANDONED, INCOMPLETE
}
