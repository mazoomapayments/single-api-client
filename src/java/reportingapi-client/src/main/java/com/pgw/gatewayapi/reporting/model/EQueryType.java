package com.pgw.gatewayapi.reporting.model;

public enum EQueryType {
    TRANSACTION, RETURN
}
