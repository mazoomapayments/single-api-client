package com.pgw.gatewayapi.reporting.model;

public enum ESettlementCode {
    NSF1, NSF2, FINAL_RETURN
}
