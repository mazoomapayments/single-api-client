package com.pgw.gatewayapi.reporting.model;

public enum ETransactionType {
    DEBIT, CREDIT, VERIFY
}
