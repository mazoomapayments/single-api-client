package com.pgw.gatewayapi.reporting.model;

import java.io.Serializable;
import java.util.List;

public class Merchant implements Serializable {

    private static final long serialVersionUID = -8286423239653398053L;

    private List<String> merchantList;

    private String merchantCustomerId;

    private List<String> merchantTransactionList;

    public List<String> getMerchantList() {
        return merchantList;
    }

    public void setMerchantList(List<String> merchantList) {
        this.merchantList = merchantList;
    }

    public String getMerchantCustomerId() {
        return merchantCustomerId;
    }

    public void setMerchantCustomerId(String merchantCustomerId) {
        this.merchantCustomerId = merchantCustomerId;
    }

    public List<String> getMerchantTransactionList() {
        return merchantTransactionList;
    }

    public void setMerchantTransactionList(List<String> merchantTransactionList) {
        this.merchantTransactionList = merchantTransactionList;
    }
}
