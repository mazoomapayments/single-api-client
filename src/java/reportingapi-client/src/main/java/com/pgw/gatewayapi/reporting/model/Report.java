package com.pgw.gatewayapi.reporting.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.io.Serializable;
import java.time.OffsetDateTime;

public class Report implements Serializable {

    private static final long serialVersionUID = -5882376885056089722L;

    private EQueryType queryType;
    private EOutputType outputType;
    private OffsetDateTime startDateTime;
    private OffsetDateTime endDateTime;

    private boolean outputZipFile = false;

    @JsonCreator
    public Report(EQueryType queryType, EOutputType outputType, OffsetDateTime startDateTime, OffsetDateTime endDateTime) {
        this.queryType = queryType;
        this.outputType = outputType;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
    }

    public EQueryType getQueryType() {
        return queryType;
    }

    public void setQueryType(EQueryType queryType) {
        this.queryType = queryType;
    }

    public EOutputType getOutputType() {
        return outputType;
    }

    public void setOutputType(EOutputType outputType) {
        this.outputType = outputType;
    }

    public OffsetDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(OffsetDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public OffsetDateTime getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(OffsetDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    public boolean isOutputZipFile() {
        return outputZipFile;
    }

    public void setOutputZipFile(boolean outputZipFile) {
        this.outputZipFile = outputZipFile;
    }
}
