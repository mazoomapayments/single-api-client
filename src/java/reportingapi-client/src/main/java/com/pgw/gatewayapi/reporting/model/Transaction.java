package com.pgw.gatewayapi.reporting.model;

import java.io.Serializable;

public class Transaction implements Serializable {

    private static final long serialVersionUID = -6773116661051726610L;

    private ETransactionType transactionType;
    private EProcessedStatus transactionStatus;
    private String transactionId;
    private String productUserId;
    private String currencyCode;
    private EDeviceType deviceType;

    public Transaction() {
    }

    public Transaction(ETransactionType transactionType, EProcessedStatus transactionStatus, String transactionId, String productUserId, String currencyCode, EDeviceType deviceType) {
        this.transactionType = transactionType;
        this.transactionStatus = transactionStatus;
        this.transactionId = transactionId;
        this.productUserId = productUserId;
        this.currencyCode = currencyCode;
        this.deviceType = deviceType;
    }

    public ETransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(ETransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public EProcessedStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(EProcessedStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getProductUserId() {
        return productUserId;
    }

    public void setProductUserId(String productUserId) {
        this.productUserId = productUserId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public EDeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(EDeviceType deviceType) {
        this.deviceType = deviceType;
    }
}
