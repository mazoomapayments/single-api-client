package com.pgw.gatewayapi.reporting.model.request;

import com.pgw.gatewayapi.reporting.model.EProduct;
import com.pgw.gatewayapi.reporting.model.Merchant;
import com.pgw.gatewayapi.reporting.model.Report;
import com.pgw.gatewayapi.reporting.model.Transaction;
import java.io.Serializable;
import java.util.List;

public class ReportRequest implements Serializable {

    private static final long serialVersionUID = -5760117400798548854L;

    private List<EProduct> productList;
    private Report report;
    private Merchant merchant;
    private Transaction transaction;
    public List<EProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<EProduct> productList) {
        this.productList = productList;
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
