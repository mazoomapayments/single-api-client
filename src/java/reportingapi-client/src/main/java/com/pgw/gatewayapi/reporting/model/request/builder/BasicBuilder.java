package com.pgw.gatewayapi.reporting.model.request.builder;

import java.util.function.Supplier;

/**
 * Basic builder template
 * @author Lawrence Li
 **/
abstract class BasicBuilder<T> {
    T target;
    private Supplier<T> supplier;

    BasicBuilder(Supplier<T> supplier) {
        this.supplier = supplier;
    }

    T getTarget() {
        if (target == null) {
            target = supplier.get();
        }
        return target;
    }

    public T build() {
        return target;
    }
}
