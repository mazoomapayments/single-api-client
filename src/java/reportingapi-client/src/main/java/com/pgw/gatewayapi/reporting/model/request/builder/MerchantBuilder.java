package com.pgw.gatewayapi.reporting.model.request.builder;

import com.pgw.gatewayapi.reporting.model.Merchant;
import java.util.List;

/**
 * Builder class to create a merchant object for reporting purpose. <p></p>
 *
 * To obtain a merchant object
 * <pre>
 *     Merchant merchant = new MerchantBuilder()
 *                          .merchantList(MERCHANT_LIST)
 *                          .customerId(MERCHANT_CUSTOMER_ID)
 *                          .transactionIdList(MERCHANT_TRANSACTION_ID_LIST)
 *                          .build();
 * </pre>
 * @author Lawrencd Li
 *
 **/
public class MerchantBuilder extends BasicBuilder<Merchant>{

    public MerchantBuilder() {
        super(Merchant::new);
    }

    public MerchantBuilder merchantList(List<String> merchantList) {
        getTarget().setMerchantList(merchantList);
        return this;
    }

    public MerchantBuilder customerId(String customerId) {
        getTarget().setMerchantCustomerId(customerId);
        return this;
    }

    public MerchantBuilder transactionIdList(List<String> transactionIdList) {
        getTarget().setMerchantTransactionList(transactionIdList);
        return this;
    }
}
