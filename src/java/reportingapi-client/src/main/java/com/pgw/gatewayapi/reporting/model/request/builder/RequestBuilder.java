package com.pgw.gatewayapi.reporting.model.request.builder;

import com.pgw.gatewayapi.reporting.model.EDeviceType;
import com.pgw.gatewayapi.reporting.model.EOutputType;
import com.pgw.gatewayapi.reporting.model.EProcessedStatus;
import com.pgw.gatewayapi.reporting.model.EProduct;
import com.pgw.gatewayapi.reporting.model.EQueryType;
import com.pgw.gatewayapi.reporting.model.ETransactionType;
import com.pgw.gatewayapi.reporting.model.Report;
import com.pgw.gatewayapi.reporting.model.request.ReportRequest;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Builder for reporting api request <p></p>
 *
 * To obtain a report request object
 * <pre>
 *         // report search condition must be specified
 *         Instant endTime = Instant.now();
 *         Instant startTime = endTime.minus(1, ChronoUnit.DAYS);
 *         // mandatory parameters
 *         // query type: TRANSACTION OR RETURN
 *         // output type: csv or json file
 *         // start time, end time: the time range for the transaction
 *         RequestBuilder builder = RequestBuilder.of(QUERY_TYPE, OUTPUT_TYPE, startTime, endTime);
 *         // or RequestBuilder builder = RequestBuilder.transactionReport(OUTPUT_TYPE, startTime, endTime);
 *         // or RequestBuilder builder = RequestBuilder.returnReport(OUTPUT_TYPE, startTime, endTime);
 *         ReportRequest request = builder
 *                 .compressed(true)                                       //optional, it is possible to compress the returned report file to zip
 *                 .productList(PRODUCT_LIST)                              //optional, a list of product
 *                 .merchantList(MERCHANT_LIST)                            //optional, a list of merchant ids
 *                 .customerId(MERCHANT_CUSTOMER_ID)                       //optional, the merchant customer id
 *                 .merchantTransactionIdList(MERCHANT_TRANSACTION_LIST)   //optional, a list of merchant transaction ids
 *                 .transactionType(TRANS_TYPE)                            //optional, transaction type: DEBIT, CREDIT or VERIFY
 *                 .transactionStatus(STATUS)                              //optional, status: PENDING, SUCCESSFUL, FAILED, REJECTED, DECLINED, CANCELED, ABANDONED, INCOMPLETE
 *                 .productTransactionId(TRANS_ID)                         //optional
 *                 .productUserId(USER_ID)                                 //optional, the user id of any product
 *                 .currencyCode(CURRENCY)                                 //optional, the currency used for the transactions
 *                 .deviceType(DEVICE_TYPE)                                //optional, the device from which a consume issues a transaction - DESKTOP, MOBILE, TABLET OR UNKNOWN
 *                 .build();
 * </pre>
 * @author Lawrence Li
 **/
public class RequestBuilder {

    private Report report;
    private MerchantBuilder merchantBuilder;
    private TransactionBuilder transactionBuilder;
    private ReportRequest request = new ReportRequest();

    private RequestBuilder(EQueryType queryType, EOutputType outputType, OffsetDateTime startDateTime, OffsetDateTime endDateTime) {
        report = new Report(queryType, outputType, startDateTime, endDateTime);
    }

    public static RequestBuilder of(EQueryType queryType, EOutputType outputType, OffsetDateTime startDateTime, OffsetDateTime endDateTime) {
        Objects.requireNonNull(queryType, "query type is required");
        Objects.requireNonNull(outputType, "output type is required");
        Objects.requireNonNull(startDateTime, "report start time required");
        Objects.requireNonNull(endDateTime, "report finish time required");

        return new RequestBuilder(queryType, outputType, startDateTime, endDateTime);
    }

    public static RequestBuilder transactionReport(EOutputType outputType, OffsetDateTime startDateTime, OffsetDateTime endDateTime) {
        return of(EQueryType.TRANSACTION, outputType, startDateTime, endDateTime);
    }

    public static RequestBuilder returnReport(EOutputType outputType, OffsetDateTime startDateTime, OffsetDateTime endDateTime) {
        return of(EQueryType.RETURN, outputType, startDateTime, endDateTime);
    }

    public RequestBuilder compressed(boolean compressed) {
        report.setOutputZipFile(compressed);
        return this;
    }

    public RequestBuilder productList(List<EProduct> products) {
        request.setProductList(products);
        return this;
    }

    public RequestBuilder merchantList(List<String> merchantList) {
        getMerchantBuilder().merchantList(merchantList);
        return this;
    }

    public RequestBuilder customerId(String customerId) {
        getMerchantBuilder().customerId(customerId);
        return this;
    }

    public RequestBuilder merchantTransactionIdList(List<String> transactionIdList) {
        getMerchantBuilder().transactionIdList(transactionIdList);
        return this;
    }

    public RequestBuilder transactionType(ETransactionType type) {
        getTransactionBuilder().transactionType(type);
        return this;
    }

    public RequestBuilder transactionStatus(EProcessedStatus status) {
        getTransactionBuilder().transactionStatus(status);
        return this;
    }

    public RequestBuilder productTransactionId(String transactionId) {
        getTransactionBuilder().transactionId(transactionId);
        return this;
    }

    public RequestBuilder productUserId(String productUserId) {
        getTransactionBuilder().productUserId(productUserId);
        return this;
    }

    public RequestBuilder currencyCode(String currencyCode) {
        getTransactionBuilder().currencyCode(currencyCode);
        return this;
    }

    public RequestBuilder deviceType(EDeviceType deviceType) {
        getTransactionBuilder().deviceType(deviceType);
        return this;
    }

    private MerchantBuilder getMerchantBuilder() {
        if (merchantBuilder == null) {
            merchantBuilder = new MerchantBuilder();
        }
        return merchantBuilder;
    }

    private TransactionBuilder getTransactionBuilder() {
        if (transactionBuilder == null) {
            transactionBuilder = new TransactionBuilder();
        }
        return transactionBuilder;
    }

    public ReportRequest build() {
        request.setReport(report);
        if (merchantBuilder != null) {
            request.setMerchant(merchantBuilder.build());
        }
        if (transactionBuilder != null) {
            request.setTransaction(transactionBuilder.build());
        }
        return request;
    }
}
