package com.pgw.gatewayapi.reporting.model.request.builder;

import com.pgw.gatewayapi.reporting.model.EDeviceType;
import com.pgw.gatewayapi.reporting.model.EProcessedStatus;
import com.pgw.gatewayapi.reporting.model.ETransactionType;
import com.pgw.gatewayapi.reporting.model.Transaction;

/**
 * Builder class to create a transaction object for reporting purpose. <p></p>
 *
 * To obtain a transaction object
 * <pre>
 *     Transaction transaction = new TransactionBuilder()
 *                                  .transactionType(TRANSACTION_TYPE)
 *                                  .transactionStatus(TRANSACTION_STATUS)
 *                                  .transactionId(PRODUCT_TRANSACTION_ID)
 *                                  .productUserId(PRODUCT_USER_ID)
 *                                  .currencyCode(CURRENCY_CODE)
 *                                  .deviceType(DEVICE_TYPE)
 *                                  .build();
 * </pre>
 * @author Lawrence Li
 *
 **/
public class TransactionBuilder extends BasicBuilder<Transaction> {

    public TransactionBuilder() {
        super(Transaction::new);
    }

    public TransactionBuilder transactionType(ETransactionType type) {
        getTarget().setTransactionType(type);
        return this;
    }

    public TransactionBuilder transactionStatus(EProcessedStatus status) {
        getTarget().setTransactionStatus(status);
        return this;
    }

    public TransactionBuilder transactionId(String transactionId) {
        getTarget().setTransactionId(transactionId);
        return this;
    }

    public TransactionBuilder productUserId(String productUserId) {
        getTarget().setProductUserId(productUserId);
        return this;
    }

    public TransactionBuilder currencyCode(String currencyCode) {
        getTarget().setCurrencyCode(currencyCode);
        return this;
    }

    public TransactionBuilder deviceType(EDeviceType deviceType) {
        getTarget().setDeviceType(deviceType);
        return this;
    }
}
