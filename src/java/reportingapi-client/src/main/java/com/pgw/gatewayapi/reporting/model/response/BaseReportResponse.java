package com.pgw.gatewayapi.reporting.model.response;

import com.pgw.gatewayapi.reporting.model.EDeviceType;
import com.pgw.gatewayapi.reporting.model.EProduct;
import java.io.Serializable;

public class BaseReportResponse implements Serializable {

    private static final long serialVersionUID = 1616847042936724242L;

    private long requestId;
    private EProduct product;
    private String merchantId;
    private String merchantUserId;
    private String merchantTransactionId;
    private String productUserId;

    private EDeviceType deviceType;

    private String transactionId;
    private String transactionNotifyStatus;
    private String transactionRegion;
    private long transactionAmount;
    private String currencyCode;
    private String countryCode;

    public long getRequestId() {
        return requestId;
    }

    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }

    public EProduct getProduct() {
        return product;
    }

    public void setProduct(EProduct product) {
        this.product = product;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantUserId() {
        return merchantUserId;
    }

    public void setMerchantUserId(String merchantUserId) {
        this.merchantUserId = merchantUserId;
    }

    public String getMerchantTransactionId() {
        return merchantTransactionId;
    }

    public void setMerchantTransactionId(String merchantTransactionId) {
        this.merchantTransactionId = merchantTransactionId;
    }

    public String getProductUserId() {
        return productUserId;
    }

    public void setProductUserId(String productUserId) {
        this.productUserId = productUserId;
    }

    public EDeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(EDeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionNotifyStatus() {
        return transactionNotifyStatus;
    }

    public void setTransactionNotifyStatus(String transactionNotifyStatus) {
        this.transactionNotifyStatus = transactionNotifyStatus;
    }

    public String getTransactionRegion() {
        return transactionRegion;
    }

    public void setTransactionRegion(String transactionRegion) {
        this.transactionRegion = transactionRegion;
    }

    public long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String toString() {
        return "requestId='" + requestId + '\'' +
                ", product='" + product + '\'' +
                ", merchantId='" + merchantId + '\'' +
                ", merchantUserId='" + merchantUserId + '\'' +
                ", merchantTransactionId='" + merchantTransactionId + '\'' +
                ", productUserId='" + productUserId + '\'' +
                ", deviceType=" + deviceType +
                ", transactionId='" + transactionId + '\'' +
                ", transactionNotifyStatus='" + transactionNotifyStatus + '\'' +
                ", transactionRegion='" + transactionRegion + '\'' +
                ", transactionAmount=" + transactionAmount +
                ", currencyCode='" + currencyCode + '\'' +
                ", countryCode='" + countryCode + '\'';
    }
}
