package com.pgw.gatewayapi.reporting.model.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pgw.gatewayapi.reporting.model.ESettlementCode;
import com.pgw.gatewayapi.reporting.model.ETransactionType;
import java.time.LocalDate;
import java.time.OffsetDateTime;

public class ReturnReportResponse extends BaseReportResponse {

    private static final long serialVersionUID = -7131738465198017739L;

    private String originalTransactionId;
    private ETransactionType originalTransactionType;
    private String originalTransactionSubType;
    private OffsetDateTime originalTransactionDateTime;
    private String originalExternalReference;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate reportDate;
    private String returnCode;
    private String nextAction;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate nextActionDate;
    private ESettlementCode settlementCode;

    public String getOriginalTransactionId() {
        return originalTransactionId;
    }

    public void setOriginalTransactionId(String originalTransactionId) {
        this.originalTransactionId = originalTransactionId;
    }

    public ETransactionType getOriginalTransactionType() {
        return originalTransactionType;
    }

    public void setOriginalTransactionType(ETransactionType originalTransactionType) {
        this.originalTransactionType = originalTransactionType;
    }

    public String getOriginalTransactionSubType() {
        return originalTransactionSubType;
    }

    public void setOriginalTransactionSubType(String originalTransactionSubType) {
        this.originalTransactionSubType = originalTransactionSubType;
    }

    public OffsetDateTime getOriginalTransactionDateTime() {
        return originalTransactionDateTime;
    }

    public void setOriginalTransactionDateTime(OffsetDateTime originalTransactionDateTime) {
        this.originalTransactionDateTime = originalTransactionDateTime;
    }

    public String getOriginalExternalReference() {
        return originalExternalReference;
    }

    public void setOriginalExternalReference(String originalExternalReference) {
        this.originalExternalReference = originalExternalReference;
    }

    public LocalDate getReportDate() {
        return reportDate;
    }

    public void setReportDate(LocalDate reportDate) {
        this.reportDate = reportDate;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getNextAction() {
        return nextAction;
    }

    public void setNextAction(String nextAction) {
        this.nextAction = nextAction;
    }

    public LocalDate getNextActionDate() {
        return nextActionDate;
    }

    public void setNextActionDate(LocalDate nextActionDate) {
        this.nextActionDate = nextActionDate;
    }

    public ESettlementCode getSettlementCode() {
        return settlementCode;
    }

    public void setSettlementCode(ESettlementCode settlementCode) {
        this.settlementCode = settlementCode;
    }

    @Override
    public String toString() {
        return "ReturnReportResponse{" +
                super.toString() + ", " +
                "originalTransactionId='" + originalTransactionId + '\'' +
                ", originalTransactionType=" + originalTransactionType +
                ", originalTransactionSubType='" + originalTransactionSubType + '\'' +
                ", originalTransactionDateTime=" + originalTransactionDateTime +
                ", originalExternalReference='" + originalExternalReference + '\'' +
                ", reportDate=" + reportDate +
                ", returnCode='" + returnCode + '\'' +
                ", nextAction='" + nextAction + '\'' +
                ", nextActionDate=" + nextActionDate +
                ", settlementCode=" + settlementCode +
                '}';
    }
}
