package com.pgw.gatewayapi.reporting.model.response;

import com.pgw.gatewayapi.reporting.model.EProcessedStatus;
import com.pgw.gatewayapi.reporting.model.ETransactionType;
import java.time.OffsetDateTime;

public class TransactionReportResponse extends BaseReportResponse {

    private static final long serialVersionUID = 3934014372138690466L;

    private String gatewayErrorCode;
    private String gatewayErrorDescription;
    private ETransactionType transactionType;
    private String transactionSubType;
    private EProcessedStatus transactionStatus;
    private long transactionFee;

//    @Schema(implementation = String.class, example = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    private OffsetDateTime transactionDateTime;

    private String externalReference;
    private String errorCode;
    private String errorDescription;

    public String getGatewayErrorCode() {
        return gatewayErrorCode;
    }

    public void setGatewayErrorCode(String gatewayErrorCode) {
        this.gatewayErrorCode = gatewayErrorCode;
    }

    public String getGatewayErrorDescription() {
        return gatewayErrorDescription;
    }

    public void setGatewayErrorDescription(String gatewayErrorDescription) {
        this.gatewayErrorDescription = gatewayErrorDescription;
    }

    public ETransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(ETransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionSubType() {
        return transactionSubType;
    }

    public void setTransactionSubType(String transactionSubType) {
        this.transactionSubType = transactionSubType;
    }

    public EProcessedStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(EProcessedStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public long getTransactionFee() {
        return transactionFee;
    }

    public void setTransactionFee(long transactionFee) {
        this.transactionFee = transactionFee;
    }

    public OffsetDateTime getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(OffsetDateTime transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public String getExternalReference() {
        return externalReference;
    }

    public void setExternalReference(String externalReference) {
        this.externalReference = externalReference;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    @Override
    public String toString() {
        return "TransactionReportResponse{" +
                super.toString() + ", " +
                "gatewayErrorCode='" + gatewayErrorCode + '\'' +
                ", gatewayErrorDescription='" + gatewayErrorDescription + '\'' +
                ", transactionType=" + transactionType +
                ", transactionSubType='" + transactionSubType + '\'' +
                ", transactionStatus=" + transactionStatus +
                ", transactionFee=" + transactionFee +
                ", transactionDateTime=" + transactionDateTime +
                ", externalReference='" + externalReference + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", errorDescription='" + errorDescription + '\'' +
                '}';
    }
}
