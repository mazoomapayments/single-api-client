package com.pgw.gatewayapi.transaction;

import com.pgw.gatewayapi.common.auth.ISecurityContext;
import com.pgw.gatewayapi.common.support.StandardClientFactory;
import com.pgw.gatewayapi.transaction.impl.TransactionApi;
import com.pgw.gatewayapi.transaction.impl.TransactionClient;

/**
 * Client factory for transaction api. A transaction api client has to be created by this factory. The required arguments are: <p></p>
 * endpoint - the base URL of transaction api (https://staging.mazoomagateway.com/transaction/ and etc.) <p></p>
 * authUrl - the base URL of the oauth service (https://staging.mazoomagateway.com/register/ and etc.) <p></p>
 * clientId, clientSecret, appKey - The security credential issued by mazooma <p></p>
 * To obtain a transaction api client
 * <pre>
 *     ITransactionClient client = new ClientFactory(ENDPOINT, AUTHURL, CLIENTID, CLIENTSECRET, APPKEY).create();
 * </pre>
 * @author Lawrence Li
 */
public class ClientFactory extends StandardClientFactory<TransactionApi, ITransactionClient> {

    public ClientFactory(String endpoint, String authUrl, String clientId, String clientSecret, String appKey) {
        super(endpoint, authUrl, clientId, clientSecret, appKey);
    }

    @Override
    public Class<TransactionApi> getApiClass() {
        return TransactionApi.class;
    }

    @Override
    public ITransactionClient createClient(TransactionApi transactionApi, ISecurityContext securityContext, String clientId, String appKey) {
        return new TransactionClient(transactionApi, securityContext, clientId, appKey);
    }
}
