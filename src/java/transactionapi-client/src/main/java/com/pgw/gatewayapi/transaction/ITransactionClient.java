package com.pgw.gatewayapi.transaction;

import com.pgw.gatewayapi.common.exception.GateWayApiException;
import com.pgw.gatewayapi.transaction.model.request.DirectTransactionRequest;
import com.pgw.gatewayapi.transaction.model.request.TransactionRequest;
import com.pgw.gatewayapi.transaction.model.response.DirectTransactionResponse;
import com.pgw.gatewayapi.transaction.model.response.TransactionResponse;
import java.util.concurrent.CompletableFuture;

/**
 * An interface that process transactions with gateway transaction api <p></p>
 *
 * Transactions are classified to three types - DEBIT (payin), CREDIT (payout) and VERIFY. A transactions type can be processed in
 * direct or interactive way.
 * @author Lawrence Li
 */
public interface ITransactionClient {

    /**
     * Make an interactive transaction. If the request is successfully handled. A transaction url is returned which points to
     * a dedicated payment app. When redirected to this url, the payment app will guide a consumer to finish the payment step by step
     */
    TransactionResponse transaction(TransactionRequest request) throws GateWayApiException;

    CompletableFuture<TransactionResponse> transactionAsync(TransactionRequest request);

    DirectTransactionResponse directTransaction(DirectTransactionRequest request) throws GateWayApiException;

    CompletableFuture<DirectTransactionResponse> directTransactionAsync(DirectTransactionRequest request);
}
