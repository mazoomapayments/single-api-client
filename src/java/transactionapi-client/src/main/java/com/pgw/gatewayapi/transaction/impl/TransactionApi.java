package com.pgw.gatewayapi.transaction.impl;

import com.pgw.gatewayapi.transaction.model.request.DirectTransactionRequest;
import com.pgw.gatewayapi.transaction.model.request.TransactionRequest;
import com.pgw.gatewayapi.transaction.model.response.DirectTransactionResponse;
import com.pgw.gatewayapi.transaction.model.response.TransactionResponse;
import java.util.concurrent.CompletableFuture;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface TransactionApi {
    @POST("v4/interactive")
    CompletableFuture<TransactionResponse> transaction(@Header("x-pgw-client-id") String clientId,
            @Header("x-pgw-app-id") String appKey,
            @Header("Authorization") String token,
            @Body TransactionRequest request);

    @POST("v4/direct")
    CompletableFuture<DirectTransactionResponse> directTransaction(@Header("x-pgw-client-id") String clientId,
            @Header("x-pgw-app-id") String appKey,
            @Header("Authorization") String token,
            @Body DirectTransactionRequest request);
}
