package com.pgw.gatewayapi.transaction.impl;

import com.pgw.gatewayapi.common.auth.ISecurityContext;
import com.pgw.gatewayapi.common.exception.GateWayApiException;
import com.pgw.gatewayapi.transaction.ITransactionClient;
import com.pgw.gatewayapi.transaction.model.request.DirectTransactionRequest;
import com.pgw.gatewayapi.transaction.model.request.TransactionRequest;
import com.pgw.gatewayapi.transaction.model.response.DirectTransactionResponse;
import com.pgw.gatewayapi.transaction.model.response.TransactionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;

import static com.pgw.gatewayapi.common.util.CommonUtils.*;

public class TransactionClient implements ITransactionClient {

    private static final Logger logger = LoggerFactory.getLogger(TransactionClient.class);

    private TransactionApi api;
    private ISecurityContext auth;
    private String clientId;
    private String appKey;

    public TransactionClient(TransactionApi api, ISecurityContext auth, String clientId, String appKey) {
        this.api = api;
        this.auth = auth;
        this.clientId = clientId;
        this.appKey = appKey;
    }

    @Override
    public TransactionResponse transaction(TransactionRequest request) throws GateWayApiException {
        return sync(transactionAsync(request));
    }

    @Override
    public DirectTransactionResponse directTransaction(DirectTransactionRequest request) throws GateWayApiException {
        return sync(directTransactionAsync(request));
    }

    public CompletableFuture<TransactionResponse> transactionAsync(TransactionRequest request) {
        return retry(logger, auth,null, 2, credential -> api.transaction(clientId, appKey, makeToken(credential), request));
    }

    @Override
    public CompletableFuture<DirectTransactionResponse> directTransactionAsync(DirectTransactionRequest request) {
        return retry(logger, auth,null, 2, credential -> api.directTransaction(clientId, appKey, makeToken(credential), request));
    }
}
