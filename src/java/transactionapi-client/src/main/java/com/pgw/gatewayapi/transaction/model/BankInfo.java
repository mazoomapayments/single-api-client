package com.pgw.gatewayapi.transaction.model;

import java.io.Serializable;

public abstract class BankInfo implements Serializable {
    public abstract EBankInformationType getBankInformationType();
}
