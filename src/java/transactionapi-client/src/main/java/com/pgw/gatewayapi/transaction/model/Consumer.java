package com.pgw.gatewayapi.transaction.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.LocalDate;

public class Consumer implements Serializable {

    private static final long serialVersionUID = -7418491741575090830L;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate dob;
    private EConsumerType consumerType;
    private IContact contactInfo;
    private String driversLicense;
    private String driversLicenseState;
    private String phoneNumber;

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public EConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(EConsumerType consumerType) {
        this.consumerType = consumerType;
    }

    public IContact getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(IContact contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getDriversLicense() {
        return driversLicense;
    }

    public void setDriversLicense(String driversLicense) {
        this.driversLicense = driversLicense;
    }

    public String getDriversLicenseState() {
        return driversLicenseState;
    }

    public void setDriversLicenseState(String driversLicenseState) {
        this.driversLicenseState = driversLicenseState;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}