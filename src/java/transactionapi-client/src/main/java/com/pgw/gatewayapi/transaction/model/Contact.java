package com.pgw.gatewayapi.transaction.model;

import java.io.Serializable;

public class Contact implements Serializable {

    private static final long serialVersionUID = 1931401451378128491L;
    private ENameType nameType;
    private IContactName name;
    private String address;
    private String address2;
    private String region;
    private String city;
    private String postalCode;
    private String countryCode;

    public IContactName getName() {
        return name;
    }

    public void setName(IContactName name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public ENameType getNameType() {
        return nameType;
    }

    public void setNameType(ENameType nameType) {
        this.nameType = nameType;
    }
}
