package com.pgw.gatewayapi.transaction.model;

import java.io.Serializable;

public class DataField implements Serializable {

    private static final long serialVersionUID = 5509965263897598998L;

    public static DataField of(String key, String value) {
        DataField fd = new DataField();
        fd.key = key;
        fd.value = value;
        return fd;
    }

    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "DataField{" +
                "key='" + key + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
