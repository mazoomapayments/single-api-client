package com.pgw.gatewayapi.transaction.model;

public class DirectConsumer extends Consumer {

    private static final long serialVersionUID = -4628376248482063659L;
    //Optional
    private String ssn;

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }
}
