package com.pgw.gatewayapi.transaction.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public class DirectMerchant implements Serializable {

    private static final long serialVersionUID = -8152386749528623746L;

    private String merchantId;
    private String merchantUserId;
    private String merchantTransactionId;
    @JsonProperty("merchantTransactionType")
    private EMerchantTransType transactionType;


    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantUserId() {
        return merchantUserId;
    }

    public void setMerchantUserId(String merchantUserId) {
        this.merchantUserId = merchantUserId;
    }

    public String getMerchantTransactionId() {
        return merchantTransactionId;
    }

    public void setMerchantTransactionId(String merchantTransactionId) {
        this.merchantTransactionId = merchantTransactionId;
    }

    public EMerchantTransType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(EMerchantTransType transactionType) {
        this.transactionType = transactionType;
    }
}
