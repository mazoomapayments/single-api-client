package com.pgw.gatewayapi.transaction.model;

public enum EBankInformationType {
    FI, TOKEN
}
