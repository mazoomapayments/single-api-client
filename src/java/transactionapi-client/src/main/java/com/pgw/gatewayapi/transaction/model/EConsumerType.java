package com.pgw.gatewayapi.transaction.model;

public enum EConsumerType {
    SENDER, RECEIVER, SENDER_RECEIVER
}
