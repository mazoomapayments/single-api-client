package com.pgw.gatewayapi.transaction.model;

public enum EMerchantTransType {
    POKER,
    CASINO,
    BINGO,
    SPORTS_BETTING,
    DIGITAL_REMITTANCE,
    E_SPORTS;
}
