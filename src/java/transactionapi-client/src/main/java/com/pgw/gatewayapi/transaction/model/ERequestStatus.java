package com.pgw.gatewayapi.transaction.model;

public enum ERequestStatus {
    SUCCESS, FAILED, REJECTED
}
