package com.pgw.gatewayapi.transaction.model;

public enum ETransactionType {
    DEBIT, CREDIT, VERIFY
}
