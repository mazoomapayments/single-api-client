package com.pgw.gatewayapi.transaction.model;

public class FiBank extends BankInfo {

    private static final long serialVersionUID = 1163064682409284441L;
    private String fiRouting;
    private String fiAccount;
    private String fiAccountType;

    @Override
    public EBankInformationType getBankInformationType() {
        return EBankInformationType.FI;
    }

    public String getFiRouting() {
        return fiRouting;
    }

    public void setFiRouting(String fiRouting) {
        this.fiRouting = fiRouting;
    }

    public String getFiAccount() {
        return fiAccount;
    }

    public void setFiAccount(String fiAccount) {
        this.fiAccount = fiAccount;
    }

    public String getFiAccountType() {
        return fiAccountType;
    }

    public void setFiAccountType(String fiAccountType) {
        this.fiAccountType = fiAccountType;
    }
}
