package com.pgw.gatewayapi.transaction.model;

public class FullName implements IContactName {

    private static final long serialVersionUID = -2721936044030245181L;
    private String fullName;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
