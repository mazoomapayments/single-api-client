package com.pgw.gatewayapi.transaction.model;

public class Merchant extends DirectMerchant {

    private static final long serialVersionUID = 3539178473121680954L;

    private Integer merchantSubId;

    public Integer getMerchantSubId() {
        return merchantSubId;
    }

    public void setMerchantSubId(Integer merchantSubId) {
        this.merchantSubId = merchantSubId;
    }
}
