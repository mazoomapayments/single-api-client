package com.pgw.gatewayapi.transaction.model;

public class Receiver implements IContact {

    private static final long serialVersionUID = -733742474434375252L;
    private Contact receiver;

    public Contact getReceiver() {
        return receiver;
    }

    public void setReceiver(Contact receiver) {
        this.receiver = receiver;
    }
}
