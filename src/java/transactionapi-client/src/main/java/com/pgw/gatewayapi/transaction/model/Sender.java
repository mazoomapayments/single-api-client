package com.pgw.gatewayapi.transaction.model;

public class Sender implements IContact {

    private static final long serialVersionUID = 8735431699788200469L;
    private Contact sender;

    public Contact getSender() {
        return sender;
    }

    public void setSender(Contact sender) {
        this.sender = sender;
    }
}

