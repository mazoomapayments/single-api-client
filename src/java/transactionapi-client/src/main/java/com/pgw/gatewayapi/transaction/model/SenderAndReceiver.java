package com.pgw.gatewayapi.transaction.model;

public class SenderAndReceiver implements IContact {

    private static final long serialVersionUID = 4198794003121219725L;
    private Contact sender;
    private Contact receiver;

    public Contact getSender() {
        return sender;
    }

    public void setSender(Contact sender) {
        this.sender = sender;
    }

    public Contact getReceiver() {
        return receiver;
    }

    public void setReceiver(Contact receiver) {
        this.receiver = receiver;
    }
}
