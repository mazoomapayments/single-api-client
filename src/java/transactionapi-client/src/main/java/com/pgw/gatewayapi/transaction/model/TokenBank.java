package com.pgw.gatewayapi.transaction.model;

public class TokenBank extends BankInfo {

    private static final long serialVersionUID = -6765641886282382854L;
    private String accountToken;

    @Override
    public EBankInformationType getBankInformationType() {
        return EBankInformationType.TOKEN;
    }

    public String getAccountToken() {
        return accountToken;
    }

    public void setAccountToken(String accountToken) {
        this.accountToken = accountToken;
    }
}
