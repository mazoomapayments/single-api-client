package com.pgw.gatewayapi.transaction.model.request;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.pgw.gatewayapi.transaction.model.BankInfo;
import com.pgw.gatewayapi.transaction.model.DirectConsumer;
import com.pgw.gatewayapi.transaction.model.DirectMerchant;
import com.pgw.gatewayapi.transaction.model.EProduct;
import com.pgw.gatewayapi.transaction.model.ETransactionType;
import java.io.Serializable;

@JsonTypeInfo(use = Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "transactionType")
@JsonSubTypes({
        @JsonSubTypes.Type(name = "DEBIT", value = DirectTransactionRequestDebit.class),
        @JsonSubTypes.Type(name = "CREDIT", value = DirectTransactionRequestCredit.class)
})
public abstract class DirectTransactionRequest implements Serializable {

    private EProduct product;
    private String countryCode;

    //Optional
    private String originatorIp;
    public abstract ETransactionType getTransactionType();
    private DirectMerchant merchant;
    private DirectConsumer consumer;
    private BankInfo bank;

    public EProduct getProduct() {
        return product;
    }

    public void setProduct(EProduct product) {
        this.product = product;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getOriginatorIp() {
        return originatorIp;
    }

    public void setOriginatorIp(String originatorIp) {
        this.originatorIp = originatorIp;
    }

    public DirectMerchant getMerchant() {
        return merchant;
    }

    public void setMerchant(DirectMerchant merchant) {
        this.merchant = merchant;
    }

    public DirectConsumer getConsumer() {
        return consumer;
    }

    public void setConsumer(DirectConsumer consumer) {
        this.consumer = consumer;
    }

    public BankInfo getBank() {
        return bank;
    }

    public void setBank(BankInfo bank) {
        this.bank = bank;
    }
}
