package com.pgw.gatewayapi.transaction.model.request;

import com.pgw.gatewayapi.transaction.model.ETransactionType;

public class DirectTransactionRequestCredit extends DirectTransactionRequest {

    private static final long serialVersionUID = -5166807037796578508L;

    private Long transactionAmount;
    private String currencyCode;

    @Override
    public ETransactionType getTransactionType() {
        return ETransactionType.CREDIT;
    }

    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
