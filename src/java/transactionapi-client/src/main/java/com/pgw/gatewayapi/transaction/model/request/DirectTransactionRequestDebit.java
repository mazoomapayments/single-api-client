package com.pgw.gatewayapi.transaction.model.request;

import com.pgw.gatewayapi.transaction.model.ETransactionType;

public class DirectTransactionRequestDebit extends DirectTransactionRequest {

    private static final long serialVersionUID = -1927271265500150061L;

    @Override
    public ETransactionType getTransactionType() {
        return ETransactionType.DEBIT;
    }

    private Long transactionAmount;
    private String currencyCode;

    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
