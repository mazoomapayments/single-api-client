package com.pgw.gatewayapi.transaction.model.request;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.pgw.gatewayapi.transaction.model.BankInfo;
import com.pgw.gatewayapi.transaction.model.Consumer;
import com.pgw.gatewayapi.transaction.model.EProduct;
import com.pgw.gatewayapi.transaction.model.ETransactionType;
import com.pgw.gatewayapi.transaction.model.Merchant;
import java.io.Serializable;

@JsonTypeInfo(use = Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "transactionType")
@JsonSubTypes({
        @JsonSubTypes.Type(name = "DEBIT", value = TransactionRequestDebit.class),
        @JsonSubTypes.Type(name = "CREDIT", value = TransactionRequestCredit.class),
        @JsonSubTypes.Type(name = "VERIFY", value = TransactionRequestVerify.class)
})
public abstract class TransactionRequest implements Serializable {

    private EProduct product;
    private String countryCode;
    private String transactionRegion;
    private String language;
    private String dataPassThrough;
    private String originatorIp;
    private Merchant merchant;
    private Consumer consumer;
    private BankInfo bank;

    public abstract ETransactionType getTransactionType();

    public EProduct getProduct() {
        return product;
    }

    public void setProduct(EProduct product) {
        this.product = product;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getTransactionRegion() {
        return transactionRegion;
    }

    public void setTransactionRegion(String transactionRegion) {
        this.transactionRegion = transactionRegion;
    }

    public String getOriginatorIp() {
        return originatorIp;
    }

    public void setOriginatorIp(String originatorIp) {
        this.originatorIp = originatorIp;
    }

    public String getDataPassThrough() {
        return dataPassThrough;
    }

    public void setDataPassThrough(String dataPassThrough) {
        this.dataPassThrough = dataPassThrough;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public Consumer getConsumer() {
        return consumer;
    }

    public void setConsumer(Consumer consumer) {
        this.consumer = consumer;
    }

    public BankInfo getBank() {
        return bank;
    }

    public void setBank(BankInfo bank) {
        this.bank = bank;
    }
}
