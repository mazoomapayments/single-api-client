package com.pgw.gatewayapi.transaction.model.request;

import com.pgw.gatewayapi.transaction.model.ETransactionType;

class TransactionRequestCredit extends TransactionRequest {

    private static final long serialVersionUID = -5347492744349911246L;
    private Long transactionAmount;
    private String currencyCode;

    private String productUserId;

    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getProductUserId() {
        return productUserId;
    }

    public void setProductUserId(String productUserId) {
        this.productUserId = productUserId;
    }

    @Override
    public ETransactionType getTransactionType() {
        return ETransactionType.CREDIT;
    }
}
