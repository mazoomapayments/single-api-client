package com.pgw.gatewayapi.transaction.model.request;

import com.pgw.gatewayapi.transaction.model.ETransactionType;

public class TransactionRequestDebit extends TransactionRequest {

    private static final long serialVersionUID = -3077005593813976209L;

    private Long transactionAmount;
    private String currencyCode;
    private String returnUrl;

    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    @Override
    public ETransactionType getTransactionType() {
        return ETransactionType.DEBIT;
    }
}
