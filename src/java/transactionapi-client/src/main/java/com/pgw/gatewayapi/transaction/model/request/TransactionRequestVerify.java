package com.pgw.gatewayapi.transaction.model.request;

import com.pgw.gatewayapi.transaction.model.ETransactionType;

class TransactionRequestVerify extends TransactionRequest {

    private static final long serialVersionUID = -6870642523692948414L;

    private String returnUrl;

    @Override
    public ETransactionType getTransactionType() {
        return ETransactionType.VERIFY;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }
}
