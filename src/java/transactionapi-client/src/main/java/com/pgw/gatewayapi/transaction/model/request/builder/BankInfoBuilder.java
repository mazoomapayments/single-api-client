package com.pgw.gatewayapi.transaction.model.request.builder;

import com.pgw.gatewayapi.transaction.model.BankInfo;
import com.pgw.gatewayapi.transaction.model.FiBank;
import com.pgw.gatewayapi.transaction.model.TokenBank;
import java.util.Objects;

/**
 * Builder class to create a bank info object
 *
 * To obtain a bank with account information
 * <pre>
 *     BankInfo bankInfo = BankInfoBuilder.bankWithAccount(ROUTING, ACCT, ACCT_TYPE)
 *                 .countryCode(COUNTRY).build();
 * </pre>
 *
 * To obtain a bank with token information
 * <pre>
 *     BankInfo bankInfo = BankInfoBuilder.bankWithToken(ACCOUNT_TOKEN)
 *                 .countryCode(COUNTRY).build();
 * </pre>
 *
 * @author Lawrence Li
 */

public class BankInfoBuilder {
    private BankInfo bankInfo;

    private BankInfoBuilder() {}

    public static BankInfoBuilder bankWithToken(String accountToken) {
        Objects.requireNonNull(accountToken, "Account token is required");

        BankInfoBuilder builder = new BankInfoBuilder();
        TokenBank tb = new TokenBank();
        tb.setAccountToken(accountToken);
        builder.bankInfo = tb;
        return builder;
    }

    public static BankInfoBuilder bankWithAccount(String fiRouting, String fiAccount, String fiAccountType) {
        Objects.requireNonNull(fiRouting, "Account routing number  is required");
        Objects.requireNonNull(fiAccount, "Account number is required");
        Objects.requireNonNull(fiAccountType, "Account type is required");

        BankInfoBuilder builder = new BankInfoBuilder();
        FiBank fb = new FiBank();
        fb.setFiRouting(fiRouting);
        fb.setFiAccount(fiAccount);
        fb.setFiAccountType(fiAccountType);
        builder.bankInfo = fb;
        return builder;
    }

    public BankInfo build() {
        return bankInfo;
    }
}
