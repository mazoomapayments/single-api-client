package com.pgw.gatewayapi.transaction.model.request.builder;

import com.pgw.gatewayapi.transaction.model.Consumer;
import com.pgw.gatewayapi.transaction.model.Contact;
import com.pgw.gatewayapi.transaction.model.DirectConsumer;
import com.pgw.gatewayapi.transaction.model.EConsumerType;
import com.pgw.gatewayapi.transaction.model.Receiver;
import com.pgw.gatewayapi.transaction.model.Sender;
import com.pgw.gatewayapi.transaction.model.SenderAndReceiver;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Builder class to create consumer object. There are two types of consumer class, one for interactive transaction and one for
 * direct transaction. <p></p>
 *
 * To obtain a consumer object
 * <pre>
 *      Consumer consumer = ConsumerBuilder.interactive(LocalDate.now())
 *                 .driversLicense(DRIVER_LICENSE)
 *                 .driversLicenseState(LICENSE_STATE)
 *                 .phoneNumber(PHONE)
 *                 .sender(SENDER)
 *                 .receiver(RECEIVER)
 *                 .build();
 * </pre>
 *
 * To obtain a direct consumer object
 * <pre>
 *     DirectConsumer consumer = ConsumerBuilder.direct(LocalDate.now().minusYears(20))
 *                 .driversLicense(DRIVER_LICENSE)
 *                 .driversLicenseState(LICENSE_STATE)
 *                 .phoneNumber(PHONE)
 *                 .ssn(SSN)
 *                 .sender(SENDER)
 *                 .receiver(RECEIVER)
 *                 .build();
 * </pre>
 * @param <T> Consumer or DirectConsumer class
 * @param <S> subclass of ConsumerBuilder
 *
 * @author Lawrence Li
 */
public abstract class ConsumerBuilder<T extends Consumer, S extends ConsumerBuilder<T, S>> {

    T consumer;
    S self;
    private SenderAndReceiver contactInfo;

    public static class InteractiveConsumerBuilder extends ConsumerBuilder<Consumer, InteractiveConsumerBuilder> {
        private InteractiveConsumerBuilder() {
            self = this;
        }

        @Override
        Consumer getObject() {
            return new Consumer();
        }
    }

    public static class DirectConsumerBuilder extends ConsumerBuilder<DirectConsumer, DirectConsumerBuilder> {

        private DirectConsumerBuilder() {
            self = this;
        }

        public DirectConsumerBuilder ssn(String ssn) {
            consumer.setSsn(ssn);
            return this;
        }

        @Override
        DirectConsumer getObject() {
            return new DirectConsumer();
        }
    }

    private ConsumerBuilder() {
    }

    public static InteractiveConsumerBuilder interactive(LocalDate dob) {
        Objects.requireNonNull(dob, "Date of birth is required");

        InteractiveConsumerBuilder builder = new InteractiveConsumerBuilder();
        initBuilder(builder, dob);
        return builder;
    }

    public static DirectConsumerBuilder direct(LocalDate dob) {
        Objects.requireNonNull(dob, "Date of birth is required");

        DirectConsumerBuilder builder = new DirectConsumerBuilder();
        initBuilder(builder, dob);
        return builder;
    }

    public S driversLicense(String driversLicense) {
        consumer.setDriversLicense(driversLicense);
        return self;
    }

    public S driversLicenseState(String driversLicenseState) {
        consumer.setDriversLicenseState(driversLicenseState);
        return self;
    }

    public S phoneNumber(String phoneNumber) {
        consumer.setPhoneNumber(phoneNumber);
        return self;
    }

    public S sender(Contact contact) {
        contactInfo.setSender(contact);
        return self;
    }

    public S receiver(Contact contact) {
        contactInfo.setReceiver(contact);
        return self;
    }

    public T build() {
        if (contactInfo.getSender() != null && contactInfo.getReceiver() != null) {
            consumer.setContactInfo(contactInfo);
            consumer.setConsumerType(EConsumerType.SENDER_RECEIVER);
        } else if (contactInfo.getSender() != null) {
            Sender s = new Sender();
            s.setSender(contactInfo.getSender());
            consumer.setContactInfo(s);
            consumer.setConsumerType(EConsumerType.SENDER);
        } else if (contactInfo.getReceiver() != null) {
            Receiver r = new Receiver();
            r.setReceiver(contactInfo.getReceiver());
            consumer.setContactInfo(r);
            consumer.setConsumerType(EConsumerType.RECEIVER);
        } else {
            throw new IllegalStateException("Sender or receiver must be provided");
        }
        return consumer;
    }

    abstract T getObject();

    private static <T extends Consumer> void initBuilder(ConsumerBuilder<T, ?> builder, LocalDate dob) {
        builder.consumer = builder.getObject();
        builder.consumer.setDob(dob);
        builder.contactInfo = new SenderAndReceiver();
    }

}
