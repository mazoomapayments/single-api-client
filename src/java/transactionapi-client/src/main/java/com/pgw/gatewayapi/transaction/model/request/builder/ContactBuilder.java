package com.pgw.gatewayapi.transaction.model.request.builder;

import com.pgw.gatewayapi.transaction.model.Contact;
import com.pgw.gatewayapi.transaction.model.ENameType;
import com.pgw.gatewayapi.transaction.model.FullName;
import com.pgw.gatewayapi.transaction.model.PersonName;
import java.util.Objects;

/**
 * Builder class to create contact object. <p></p>
 *
 * To obtain a contact with only full name
 * <pre>
 *      Contact contact = new ContactBuilder(ADDRE, REGION, CITY, ZIP, COUNTRY)
 *                 .name(FULL_NAME)
 *                 .build();
 * </pre>
 *
 * To obtain a contact of first name and last name
 * <pre>
 *     Contact contact = new ContactBuilder(ADDRE, REGION, CITY, ZIP, COUNTRY)
 *                 .name(FIRST_NAME, MIDDLE_NAME, LAST_NAME).build();
 * </pre>
 *
 * @author Lawrence Li
 */

public class ContactBuilder {
    private Contact contact;

    public ContactBuilder(String address, String region, String city, String postalCode, String countryCode) {
        contact = new Contact();
        contact.setAddress(address);
        contact.setRegion(region);
        contact.setCity(city);
        contact.setPostalCode(postalCode);
        contact.setCountryCode(countryCode);
    }

    public ContactBuilder address2(String addr) {
        contact.setAddress2(addr);
        return this;
    }

    public ContactBuilder name(String fullName) {
        FullName fn = new FullName();
        fn.setFullName(fullName);
        contact.setName(fn);
        contact.setNameType(ENameType.FULL);
        return this;
    }

    public ContactBuilder name(String fistName, String middleName, String lastName) {
        PersonName pn = new PersonName();
        pn.setFirstName(fistName);
        pn.setMiddleName(middleName);
        pn.setLastName(lastName);
        contact.setName(pn);
        contact.setNameType(ENameType.PERSON);
        return this;
    }

    public Contact build() {
        Objects.requireNonNull(contact.getName(), "Name is required");
        return contact;
    }
}
