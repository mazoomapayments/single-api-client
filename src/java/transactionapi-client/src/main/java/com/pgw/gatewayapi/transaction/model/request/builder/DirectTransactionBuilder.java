package com.pgw.gatewayapi.transaction.model.request.builder;

import com.pgw.gatewayapi.transaction.model.BankInfo;
import com.pgw.gatewayapi.transaction.model.DirectConsumer;
import com.pgw.gatewayapi.transaction.model.DirectMerchant;
import com.pgw.gatewayapi.transaction.model.EProduct;
import com.pgw.gatewayapi.transaction.model.request.DirectTransactionRequest;
import com.pgw.gatewayapi.transaction.model.request.DirectTransactionRequestCredit;
import com.pgw.gatewayapi.transaction.model.request.DirectTransactionRequestDebit;
import java.util.Objects;

public class DirectTransactionBuilder {
    private DirectTransactionRequest request;

    private DirectTransactionBuilder() {}

    public static DirectTransactionBuilder debit(EProduct product, String countryCode, Long transactionAmount, String currencyCode) {
        validate(product, countryCode, transactionAmount, currencyCode);

        DirectTransactionBuilder builder = new DirectTransactionBuilder();
        DirectTransactionRequestDebit debit = new DirectTransactionRequestDebit();
        debit.setProduct(product);
        debit.setCountryCode(countryCode);
        debit.setTransactionAmount(transactionAmount);
        debit.setCurrencyCode(currencyCode);
        builder.request = debit;
        return builder;
    }

    public static DirectTransactionBuilder credit(EProduct product, String countryCode, Long transactionAmount, String currencyCode) {
        validate(product, countryCode, transactionAmount, currencyCode);

        DirectTransactionBuilder builder = new DirectTransactionBuilder();
        DirectTransactionRequestCredit credit = new DirectTransactionRequestCredit();
        credit.setProduct(product);
        credit.setCountryCode(countryCode);
        credit.setTransactionAmount(transactionAmount);
        credit.setCurrencyCode(currencyCode);
        builder.request = credit;
        return builder;
    }



    public DirectTransactionBuilder originatorIp(String originatorIp) {
        request.setOriginatorIp(originatorIp);
        return this;
    }

    public DirectTransactionBuilder merchant(DirectMerchant merchant) {
        request.setMerchant(merchant);
        return this;
    }

    public DirectTransactionBuilder consumer(DirectConsumer consumer) {
        request.setConsumer(consumer);
        return this;
    }

    public DirectTransactionBuilder bankInfo(BankInfo bankInfo) {
        request.setBank(bankInfo);
        return this;
    }

    public DirectTransactionRequest build() {
        return request;
    }

    private static void validate(EProduct product, String countryCode, Long transactionAmount, String currencyCode) {
        Objects.requireNonNull(product, "product is required");
        Objects.requireNonNull(countryCode, "country code is required");
        Objects.requireNonNull(transactionAmount, "transaction amount is required");
        Objects.requireNonNull(currencyCode, "currency code is required");
    }
}
