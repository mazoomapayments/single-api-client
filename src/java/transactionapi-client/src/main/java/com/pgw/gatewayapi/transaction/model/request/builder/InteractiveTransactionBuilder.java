package com.pgw.gatewayapi.transaction.model.request.builder;

import com.pgw.gatewayapi.transaction.model.BankInfo;
import com.pgw.gatewayapi.transaction.model.Consumer;
import com.pgw.gatewayapi.transaction.model.EProduct;
import com.pgw.gatewayapi.transaction.model.Merchant;
import com.pgw.gatewayapi.transaction.model.request.TransactionRequest;
import com.pgw.gatewayapi.transaction.model.request.TransactionRequestDebit;
import java.util.Objects;

public class InteractiveTransactionBuilder {
    private TransactionRequest request;

    private InteractiveTransactionBuilder() {}

    public static InteractiveTransactionBuilder debit(EProduct product, String countryCode, Long transactionAmount, String currencyCode, String returnUrl) {
        Objects.requireNonNull(product, "product is required");
        Objects.requireNonNull(countryCode, "country code is required");
        Objects.requireNonNull(transactionAmount, "transaction amount is required");
        Objects.requireNonNull(currencyCode, "currency code is required");
        Objects.requireNonNull(returnUrl, "return url is required");

        InteractiveTransactionBuilder builder = new InteractiveTransactionBuilder();
        TransactionRequestDebit debit = new TransactionRequestDebit();
        debit.setProduct(product);
        debit.setCountryCode(countryCode);
        debit.setTransactionAmount(transactionAmount);
        debit.setCurrencyCode(currencyCode);
        debit.setReturnUrl(returnUrl);
        builder.request = debit;
        return builder;
    }

    public InteractiveTransactionBuilder transactionRegion(String transactionRegion) {
        request.setTransactionRegion(transactionRegion);
        return this;
    }

    public InteractiveTransactionBuilder language(String language) {
        request.setLanguage(language);
        return this;
    }

    public InteractiveTransactionBuilder dataPassThrough(String dataPassThrough) {
        request.setDataPassThrough(dataPassThrough);
        return this;
    }

    public InteractiveTransactionBuilder originatorIp(String originatorIp) {
        request.setOriginatorIp(originatorIp);
        return this;
    }

    public InteractiveTransactionBuilder merchant(Merchant merchant) {
        request.setMerchant(merchant);
        return this;
    }

    public InteractiveTransactionBuilder consumer(Consumer consumer) {
        request.setConsumer(consumer);
        return this;
    }

    public InteractiveTransactionBuilder bankInfo(BankInfo bankInfo) {
        request.setBank(bankInfo);
        return this;
    }

    public TransactionRequest build() {
        return request;
    }
}
