package com.pgw.gatewayapi.transaction.model.request.builder;

import com.pgw.gatewayapi.transaction.model.DirectMerchant;
import com.pgw.gatewayapi.transaction.model.EMerchantTransType;
import com.pgw.gatewayapi.transaction.model.Merchant;
import java.util.Objects;

public class MerchantBuilder {

    private MerchantBuilder() {
    }

    public static DirectMerchant direct(String merchantId, String merchantUserId, String merchantTransactionId, EMerchantTransType transactionType) {
        Objects.requireNonNull(merchantId, "merchant id is required");
        Objects.requireNonNull(merchantUserId, "merchant user id is required");
        Objects.requireNonNull(merchantTransactionId, "merchant transaction id is required");

        DirectMerchant merchant = new DirectMerchant();
        merchant.setMerchantId(merchantId);
        merchant.setMerchantUserId(merchantUserId);
        merchant.setMerchantTransactionId(merchantTransactionId);
        merchant.setTransactionType(transactionType);
        return merchant;
    }

    public static Merchant interactive(String merchantId, String merchantUserId, String merchantTransactionId, EMerchantTransType transactionType, Integer merchantSubId) {
        Objects.requireNonNull(merchantId, "merchant id is required");
        Objects.requireNonNull(merchantUserId, "merchant user id is required");
        Objects.requireNonNull(merchantTransactionId, "merchant transaction id is required");

        Merchant merchant = new Merchant();
        merchant.setMerchantId(merchantId);
        merchant.setMerchantUserId(merchantUserId);
        merchant.setMerchantTransactionId(merchantTransactionId);
        merchant.setTransactionType(transactionType);
        merchant.setMerchantSubId(merchantSubId);
        return merchant;
    }
}
