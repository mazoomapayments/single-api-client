package com.pgw.gatewayapi.transaction.model.request.builder;

import com.pgw.gatewayapi.transaction.model.Consumer;
import com.pgw.gatewayapi.transaction.model.Contact;
import com.pgw.gatewayapi.transaction.model.DirectConsumer;
import com.pgw.gatewayapi.transaction.model.DirectMerchant;
import com.pgw.gatewayapi.transaction.model.EConsumerType;
import com.pgw.gatewayapi.transaction.model.EMerchantTransType;
import com.pgw.gatewayapi.transaction.model.EProduct;
import com.pgw.gatewayapi.transaction.model.FiBank;
import com.pgw.gatewayapi.transaction.model.Merchant;
import com.pgw.gatewayapi.transaction.model.Receiver;
import com.pgw.gatewayapi.transaction.model.Sender;
import com.pgw.gatewayapi.transaction.model.SenderAndReceiver;
import com.pgw.gatewayapi.transaction.model.TokenBank;
import com.pgw.gatewayapi.transaction.model.request.DirectTransactionRequest;
import com.pgw.gatewayapi.transaction.model.request.DirectTransactionRequestCredit;
import com.pgw.gatewayapi.transaction.model.request.DirectTransactionRequestDebit;
import com.pgw.gatewayapi.transaction.model.request.TransactionRequest;
import com.pgw.gatewayapi.transaction.model.request.TransactionRequestDebit;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Builder class to create a transaction request. <p></p>
 *
 * A transaction request has four sections - transaction, merchant, consumer and bankinfo.
 * Each section is build with {@code withXXX()} method and connected by {@code and()} method. <p></p>
 *
 * To obtain an interactive transaction request
 * <pre>
 *     TransactionRequest request = RequestBuilder.interactive(PRODUCT, COUNTRY) // make an interactive transaction request
 *                 .debit(AMOUNT, CURRENCY, RETURN_URL) // make a debit transaction, the transaction amount, currency and return url is required
 *                 .dataPassThrough(DATA) // optional - extra data fields
 *                 .language(LANGUAGE) // optional - language
 *                 .transactionRegion(TRANSACTION_STATE) // optional - the region where the transaction takes place
 *                 .originatorIp(IP) // optional - the orginal ip address where the transaction is initiated
 *                 .withMerchant(MERCHANT_ID, MERCHANT_USER_ID, MERCHANT_TRAN_ID) // set merchant, merchant id, use id and transaction id is required
 *                 .transactionType(EMerchantTransType.CASINO) // optional - merchant transaction type
 *                 .merchantSubId(MERCHANT_SUB_ID) // optional - merchant sub id
 *                 .and()
 *                 .withConsumer(DOB) // set consumer, the date of birth is required
 *                 .driversLicense(DRIVER_LICENSE) // optional - driver's license
 *                 .driversLicenseState(LICENSE_STATE) // optional - the issue state of the driver's license
 *                 .phoneNumber(PHONE) // optional - phone number
 *                 // sender or receiver at least one of each needs to be set
 *                 .sender(new ContactBuilder(ADDRE, REGION, CITY, ZIP, COUNTRY) // set the sender of the customer, address, region, city, postalcode and country is required
 *                         .name(FULL_NAME) // use full name for contact name
 *                         .build())
 *                 .receiver(new ContactBuilder(ADDRE, REGION, CITY, ZIP, COUNTRY) // set the receiver of the customer, address, region, city, postalcode and country is required
 *                         .name(FIRST_NAME, MIDDLE_NAME, LAST_NAME).build()) // use first name, middle name and last name for contact name
 *                 .and()
 * //                .bankWithToken(TOKEN) // use access token for bank info
 *                 .bankWithAccount(ROUTING, ACCT, ACCT_TYPE) // use bank account info for bank info
 *                 .country(COUNTRY) // optional - bank's country
 *                 .and()
 *                 .build();
 * </pre>
 *
 * To obtain a direct transaction request
 * <pre>
 *      RequestBuilder.direct(PRODUCT, COUNTRY) //make a direct transaction request
 *                 .credit(AMOUNT, CURRENCY) // make a credit transaction, the transaction amount and currency is required
 *                 .originatorIp(IP) // optional - the orginal ip address where the transaction is initiated
 *                 .withMerchant(MERCHANT_ID, MERCHANT_USER_ID, MERCHANT_TRAN_ID) // set merchant, merchant id, use id and transaction id is required
 *                 .transactionType(EMerchantTransType.CASINO) // optional - merchant transaction type
 *                 .and()
 *                 .withConsumer(DOB) // set consumer, the dob is required
 *                 .driversLicense(DRIVER_LICENSE) // optional - driver's license
 *                 .driversLicenseState(LICENSE_STATE) // optional - the issue state of the driver's license
 *                 .phoneNumber(PHONE) // optional - phone number
 *                 .ssn(SSN) // optional SSN
 *                 .sender(new ContactBuilder(ADDRE, REGION, CITY, ZIP, COUNTRY) // set the sender of the consumer, address, region, city, postalcode and country is required
 *                         .name(FULL_NAME) // use full name for contact name
 *                         .build())
 *                 .receiver(new ContactBuilder(ADDRE, REGION, CITY, ZIP, COUNTRY) // set the receiver of the consumer, address, region, city, postalcode and country is required
 *                         .name(FIRST_NAME, MIDDLE_NAME, LAST_NAME).build()) // use first name, middle name and last name for contact name
 *                 .and()
 * //                .bankWithToken(TOKEN) // use access token for bank info
 *                 .bankWithAccount(ROUTING, ACCT, ACCT_TYPE) // set bank account info for bank info
 *                 .country(COUNTRY) // optional - bank's country
 *                 .and()
 *                 .build();
 * </pre>
 *
 * @author Lawrence Li
 */
public class RequestBuilder {

    public static InteractiveSpec interactive(EProduct product, String countryCode) {
        Objects.requireNonNull(product, "product is required");
        Objects.requireNonNull(countryCode, "country code is required");

        return new InteractiveSpec(product, countryCode);
    }

    public static DirectSpec direct(EProduct product, String countryCode) {
        Objects.requireNonNull(product, "product is required");
        Objects.requireNonNull(countryCode, "country code is required");

        return new DirectSpec(product, countryCode);
    }

    public static class DirectSpec {

        private EProduct product;
        private String countryCode;

        public DirectSpec(EProduct product, String countryCode) {
            this.product = product;
            this.countryCode = countryCode;
        }

        public DebitDirectTransactionSpec debit(long transactionAmount, String currencyCode) {
            Objects.requireNonNull(currencyCode, "currency code is required");

            return new DebitDirectTransactionSpec(product, countryCode, transactionAmount, currencyCode);
        }

        public CreditDirectTransactionSpec credit(long transactionAmount, String currencyCode) {
            Objects.requireNonNull(currencyCode, "currency code is required");

            return new CreditDirectTransactionSpec(product, countryCode, transactionAmount, currencyCode);
        }
    }

    public static class InteractiveSpec {

        private EProduct product;
        private String countryCode;

        public InteractiveSpec(EProduct product, String countryCode) {
            this.product = product;
            this.countryCode = countryCode;
        }

        public DebitInteractiveTransactionSpec debit(long transactionAmount, String currencyCode, String returnUrl) {
            Objects.requireNonNull(currencyCode, "currency code is required");
            Objects.requireNonNull(returnUrl, "return url is required");
            return new DebitInteractiveTransactionSpec(product, countryCode, transactionAmount, currencyCode, returnUrl);
        }
    }

    /**
     * @param <T> the type of the object created by the builder
     * @param <P> the type of the builder's parent
     */
    private static abstract class BuilderSpec<T, P> {

        private P parent;

        BuilderSpec(P parent) {
            this.parent = parent;
        }

        public P and() {
            return parent;
        }

        abstract T build();
    }

    private abstract static class TransactionSpec<R> {

        Set<java.util.function.Consumer<R>> subBuilders = new HashSet<>();

        public R build() {
            R target = doBuild();
            for (java.util.function.Consumer<R> builder : subBuilders) {
                builder.accept(target);
            }
            return target;
        }

        abstract R doBuild();
    }

    public abstract static class BaseTransactionSpec<R> extends TransactionSpec<R> {

        R target;

        @Override
        R doBuild() {
            return target;
        }

        abstract R createTarget();
    }

    public abstract static class BaseDirectTransactionSpec<R extends DirectTransactionRequest> extends BaseTransactionSpec<R> {

        BaseDirectTransactionSpec(EProduct product, String countryCode) {
            target = createTarget();
            target.setProduct(product);
            target.setCountryCode(countryCode);
        }

        public BaseDirectTransactionSpec<R> originatorIp(String originatorIp) {
            target.setOriginatorIp(originatorIp);
            return this;
        }

        public DirectMerchantSpec withMerchant(String merchantId, String merchantUserId, String merchantTransactionId) {
            Objects.requireNonNull(merchantId, "merchant id is required");
            Objects.requireNonNull(merchantUserId, "merchant user id is required");
            Objects.requireNonNull(merchantTransactionId, "merchant transaction id is required");

            DirectMerchantSpec spec = new DirectMerchantSpec(this, merchantId, merchantUserId, merchantTransactionId);
            subBuilders.add(r -> r.setMerchant(spec.build()));
            return spec;
        }

        public DirectConsumerSpec withConsumer(LocalDate dob) {
            Objects.requireNonNull(dob, "Date of birth is required");

            DirectConsumerSpec spec = new DirectConsumerSpec(this, dob);
            subBuilders.add(r -> r.setConsumer(spec.build()));
            return spec;
        }

        public TokenBankSpec<BaseDirectTransactionSpec<?>> bankWithToken(String token) {
            TokenBankSpec<BaseDirectTransactionSpec<?>> spec = new TokenBankSpec<>(this, token);
            subBuilders.add(r -> r.setBank(spec.build()));
            return spec;
        }

        public FiBankSpec<BaseDirectTransactionSpec<?>> bankWithAccount(String fiRouting, String fiAccount, String fiAccountType) {
            FiBankSpec<BaseDirectTransactionSpec<?>> spec = new FiBankSpec<>(this, fiRouting, fiAccount, fiAccountType);
            subBuilders.add(r -> r.setBank(spec.build()));
            return spec;
        }
    }

    public abstract static class BaseInteractiveTransactionSpec<R extends TransactionRequest> extends BaseTransactionSpec<R> {

        BaseInteractiveTransactionSpec(EProduct product, String countryCode) {
            target = createTarget();
            target.setProduct(product);
            target.setCountryCode(countryCode);
        }

        public BaseInteractiveTransactionSpec<R> transactionRegion(String transactionRegion) {
            target.setTransactionRegion(transactionRegion);
            return this;
        }

        public BaseInteractiveTransactionSpec<R> language(String language) {
            target.setLanguage(language);
            return this;
        }

        public BaseInteractiveTransactionSpec<R> dataPassThrough(String dataPassThrough) {
            target.setDataPassThrough(dataPassThrough);
            return this;
        }

        public BaseInteractiveTransactionSpec<R> originatorIp(String originatorIp) {
            target.setOriginatorIp(originatorIp);
            return this;
        }

        public MerchantSpec withMerchant(String merchantId, String merchantUserId, String merchantTransactionId) {
            Objects.requireNonNull(merchantId, "merchant id is required");
            Objects.requireNonNull(merchantUserId, "merchant user id is required");
            Objects.requireNonNull(merchantTransactionId, "merchant transaction id is required");

            MerchantSpec spec = new MerchantSpec(this, merchantId, merchantUserId, merchantTransactionId);
            subBuilders.add(r -> r.setMerchant(spec.build()));
            return spec;
        }

        public InteractiveConsumerSpec withConsumer(LocalDate dob) {
            Objects.requireNonNull(dob, "Date of birth is required");

            InteractiveConsumerSpec spec = new InteractiveConsumerSpec(this, dob);
            subBuilders.add(r -> r.setConsumer(spec.build()));
            return spec;
        }

        public TokenBankSpec<BaseInteractiveTransactionSpec<?>> bankWithToken(String token) {
            TokenBankSpec<BaseInteractiveTransactionSpec<?>> spec = new TokenBankSpec<>(this, token);
            subBuilders.add(r -> r.setBank(spec.build()));
            return spec;
        }

        public FiBankSpec<BaseInteractiveTransactionSpec<?>> bankWithAccount(String fiRouting, String fiAccount, String fiAccountType) {
            FiBankSpec<BaseInteractiveTransactionSpec<?>> spec = new FiBankSpec<>(this, fiRouting, fiAccount, fiAccountType);
            subBuilders.add(r -> r.setBank(spec.build()));
            return spec;
        }
    }

    public static class CreditDirectTransactionSpec extends BaseDirectTransactionSpec<DirectTransactionRequestCredit> {

        public CreditDirectTransactionSpec(EProduct product, String countryCode, long transactionAmount, String currencyCode) {
            super(product, countryCode);
            target.setTransactionAmount(transactionAmount);
            target.setCurrencyCode(currencyCode);
        }

        @Override
        DirectTransactionRequestCredit createTarget() {
            return new DirectTransactionRequestCredit();
        }

        @Override
        public DirectTransactionRequestCredit build() {
            DirectTransactionRequestCredit credit = super.build();

            if (credit.getMerchant() == null) {
                throw new IllegalStateException("Merchant info is not set");
            }

            if (credit.getConsumer() == null) {
                throw new IllegalStateException("Consumer info is not set");
            }

            return credit;
        }
    }

    public static class DebitDirectTransactionSpec extends BaseDirectTransactionSpec<DirectTransactionRequestDebit> {

        public DebitDirectTransactionSpec(EProduct product, String countryCode, long transactionAmount, String currencyCode) {
            super(product, countryCode);
            target.setTransactionAmount(transactionAmount);
            target.setCurrencyCode(currencyCode);
        }

        @Override
        DirectTransactionRequestDebit createTarget() {
            return new DirectTransactionRequestDebit();
        }

        @Override
        public DirectTransactionRequestDebit build() {
            DirectTransactionRequestDebit debit = super.build();

            if (debit.getMerchant() == null) {
                throw new IllegalStateException("Merchant info is not set");
            }

            if (debit.getConsumer() == null) {
                throw new IllegalStateException("Consumer info is not set");
            }

            return debit;
        }
    }

    public static class DebitInteractiveTransactionSpec extends BaseInteractiveTransactionSpec<TransactionRequestDebit> {

        public DebitInteractiveTransactionSpec(EProduct product, String countryCode, long transactionAmount, String currencyCode, String returnUrl) {
            super(product, countryCode);
            target.setTransactionAmount(transactionAmount);
            target.setCurrencyCode(currencyCode);
            target.setReturnUrl(returnUrl);
        }

        @Override
        TransactionRequestDebit createTarget() {
            return new TransactionRequestDebit();
        }

        @Override
        public TransactionRequestDebit build() {
            TransactionRequestDebit debit = super.build();

            if (debit.getMerchant() == null) {
                throw new IllegalStateException("Merchant info is not set");
            }

            if (debit.getConsumer() == null) {
                throw new IllegalStateException("Consumer info is not set");
            }

            return debit;
        }
    }

    public static class FiBankSpec<T extends BaseTransactionSpec<?>> extends BuilderSpec<FiBank, T> {

        private FiBank bankInfo;

        public FiBankSpec(T parent, String fiRouting, String fiAccount, String fiAccountType) {
            super(parent);
            bankInfo = new FiBank();
            bankInfo.setFiRouting(fiRouting);
            bankInfo.setFiAccount(fiAccount);
            bankInfo.setFiAccountType(fiAccountType);
        }

        @Override
        FiBank build() {
            return bankInfo;
        }
    }

    public static class TokenBankSpec<T extends BaseTransactionSpec<?>> extends BuilderSpec<TokenBank, T> {

        private TokenBank bankInfo;

        public TokenBankSpec(T parent, String token) {
            super(parent);
            bankInfo = new TokenBank();
            bankInfo.setAccountToken(token);
        }

        @Override
        TokenBank build() {
            return bankInfo;
        }
    }

    public static class DirectConsumerSpec extends ConsumerSpec<DirectConsumer, DirectConsumerSpec, BaseDirectTransactionSpec<?>> {

        public DirectConsumerSpec(BaseDirectTransactionSpec<?> parent, LocalDate dob) {
            super(parent, dob);
            self = this;
        }

        public DirectConsumerSpec ssn(String ssn) {
            consumer.setSsn(ssn);
            return this;
        }

        @Override
        DirectConsumer getObject() {
            return new DirectConsumer();
        }
    }

    public static class InteractiveConsumerSpec extends ConsumerSpec<Consumer, InteractiveConsumerSpec, BaseInteractiveTransactionSpec<?>> {

        private InteractiveConsumerSpec(BaseInteractiveTransactionSpec<?> parent, LocalDate dob) {
            super(parent, dob);
            self = this;
        }

        @Override
        Consumer getObject() {
            return new Consumer();
        }
    }

    public abstract static class ConsumerSpec<T extends Consumer, S extends ConsumerSpec<T, S, P>, P extends BaseTransactionSpec<?>> extends BuilderSpec<T, P> {

        T consumer;
        S self;
        private Contact sender;
        private Contact receiver;

        public ConsumerSpec(P parent, LocalDate dob) {
            super(parent);
            consumer = getObject();
            consumer.setDob(dob);
        }

        public S driversLicense(String driversLicense) {
            consumer.setDriversLicense(driversLicense);
            return self;
        }

        public S driversLicenseState(String driversLicenseState) {
            consumer.setDriversLicenseState(driversLicenseState);
            return self;
        }

        public S phoneNumber(String phoneNumber) {
            consumer.setPhoneNumber(phoneNumber);
            return self;
        }

        public S sender(Contact contact) {
            sender = contact;
            return self;
        }

        public S receiver(Contact contact) {
            receiver = contact;
            return self;
        }

        @Override
        T build() {
            if (sender != null && receiver != null) {
                SenderAndReceiver sr = new SenderAndReceiver();
                sr.setSender(sender);
                sr.setReceiver(receiver);
                consumer.setContactInfo(sr);
                consumer.setConsumerType(EConsumerType.SENDER_RECEIVER);
            } else if (sender != null) {
                Sender s = new Sender();
                s.setSender(sender);
                consumer.setContactInfo(s);
                consumer.setConsumerType(EConsumerType.SENDER);
            } else if (receiver != null) {
                Receiver r = new Receiver();
                r.setReceiver(receiver);
                consumer.setContactInfo(r);
                consumer.setConsumerType(EConsumerType.RECEIVER);
            } else {
                throw new IllegalStateException("Sender or receiver must be provided");
            }
            return consumer;
        }

        abstract T getObject();
    }

    public static class DirectMerchantSpec extends BuilderSpec<DirectMerchant, BaseDirectTransactionSpec<?>> {

        private DirectMerchant merchant;

        DirectMerchantSpec(BaseDirectTransactionSpec<?> parent, String merchantId, String merchantUserId, String merchantTransactionId) {
            super(parent);
            merchant = new DirectMerchant();
            merchant.setMerchantId(merchantId);
            merchant.setMerchantUserId(merchantUserId);
            merchant.setMerchantTransactionId(merchantTransactionId);
        }

        public DirectMerchantSpec transactionType(EMerchantTransType transactionType) {
            merchant.setTransactionType(transactionType);
            return this;
        }

        @Override
        DirectMerchant build() {
            return merchant;
        }
    }

    public static class MerchantSpec extends BuilderSpec<Merchant, BaseInteractiveTransactionSpec<?>> {

        private Merchant merchant;

        MerchantSpec(BaseInteractiveTransactionSpec<?> parent, String merchantId, String merchantUserId, String merchantTransactionId) {
            super(parent);
            merchant = new Merchant();
            merchant.setMerchantId(merchantId);
            merchant.setMerchantUserId(merchantUserId);
            merchant.setMerchantTransactionId(merchantTransactionId);
        }

        public MerchantSpec transactionType(EMerchantTransType transactionType) {
            merchant.setTransactionType(transactionType);
            return this;
        }

        public MerchantSpec merchantSubId(int merchantSubId) {
            merchant.setMerchantSubId(merchantSubId);
            return this;
        }

        @Override
        Merchant build() {
            return merchant;
        }
    }
}
