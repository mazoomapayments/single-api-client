package com.pgw.gatewayapi.transaction.model.response;

import com.pgw.gatewayapi.transaction.model.DataField;
import com.pgw.gatewayapi.transaction.model.ERequestStatus;
import java.io.Serializable;
import java.util.List;

public class DirectTransactionResponse implements Serializable {

    private static final long serialVersionUID = -8717808111720194660L;
    private Long requestId;
    private String transactionId;
    private long transactionFee;
    private ERequestStatus requestStatus;
    private String currencyCode;
    private String errorCode;
    private List<DataField> additionalData;

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public long getTransactionFee() {
        return transactionFee;
    }

    public void setTransactionFee(long transactionFee) {
        this.transactionFee = transactionFee;
    }

    public ERequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(ERequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public List<DataField> getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(List<DataField> additionalData) {
        this.additionalData = additionalData;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
