package com.pgw.gatewayapi.transaction.model.response;

import com.pgw.gatewayapi.transaction.model.ERequestStatus;
import java.io.Serializable;

public class TransactionResponse implements Serializable {

    private static final long serialVersionUID = 2624742145534658752L;

    private Long requestId;
    private ERequestStatus requestStatus;


    private String transactionUrl;
    private String errorCode;

    public String getTransactionUrl() {
        return transactionUrl;
    }

    public void setTransactionUrl(String transactionUrl) {
        this.transactionUrl = transactionUrl;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public ERequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(ERequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
